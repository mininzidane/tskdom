<?php

class PaginationWidget extends CLinkPager {

	public function init() {
		parent::init();

		//TODO it's hack!
		$this->nextPageLabel = preg_replace('/\s*\&gt\;/i', '', Yii::t('yii', 'Next &gt;'));
		$this->prevPageLabel = preg_replace('/\&lt\;\s*/i', '', Yii::t('yii', '&lt; Previous'));
	}
}