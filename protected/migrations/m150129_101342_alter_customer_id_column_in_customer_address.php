<?php

class m150129_101342_alter_customer_id_column_in_customer_address extends DbMigration {

	public function safeUp() {
		$this->addColumn('customer_address', 'concreteOrderId', 'VARCHAR(128) NULL DEFAULT NULL AFTER customerId');
		$this->addColumn('customer_address', 'specOrderId', 'VARCHAR(128) NULL DEFAULT NULL AFTER concreteOrderId');
	}

	public function safeDown() {
		$this->dropColumn('customer_address', 'specOrderId');
		$this->dropColumn('customer_address', 'concreteOrderId');
	}
}
