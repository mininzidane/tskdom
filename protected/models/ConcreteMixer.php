<?php

/**
 * Class Cement
 *
 * @author Lukin A.
 *
 * @property int    $id
 * @property string $mixerInfo
 * @property int    $orderId
 */
class ConcreteMixer extends ActiveRecord {

	/**
	 * @param string $className
	 * @return ConcreteMixer
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'concrete_mixer';
	}


	public function rules() {
		return [
		];
	}

	public function relations() {
		return [
		];
	}


	public function attributeLabels() {
		$values = [
		];

		return self::processLabels($values);
	}

	/**
	 * Расчитываем остатки перед записью в базу
	 *
	 * @author Lukin A.
	 *
	 * @return bool
	 */
	protected function beforeSave() {
		if (!is_string($this->mixerInfo)) {
			$this->mixerInfo = serialize($this->mixerInfo);
		}
		return parent::beforeSave();
	}

	public function getMixers() {
		if ($this->mixerInfo === null) {
			return [];
		}

		return @unserialize($this->mixerInfo);
	}

	public function getMixerNumbers() {
		$sql = 'SELECT number FROM mixer';
		return Yii::app()->db->createCommand($sql)->queryColumn();
	}
}
