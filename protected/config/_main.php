<?php

return [
	'basePath'       => __DIR__ . DIRECTORY_SEPARATOR . '..',
	'name'           => '',

	// preloading 'log' component
	'preload'        => ['log'],

	// autoloading model and component classes
	'import'         => [
		'application.controllers.BaseController',
		'application.models.*',
		'application.components.*',
		'ext.navigation.*',
		'ext.httpclient.*',
		'ext.PHPExcel.Classes.PHPExcel',
	],

	'language'       => 'ru',
	'sourceLanguage' => 'ru',

	// application components
	'components'     => [
		'urlManager'   => [
			'urlFormat'        => 'path',
			'caseSensitive'    => true,
			'matchValue'       => true,
			'showScriptName'   => false,
			'urlSuffix'        => '/',
			'useStrictParsing' => true,
			'rules'            => require('_routes.php'),
		],
		'db'           => [
			'connectionString'   => 'mysql:host=localhost;dbname=tskdom',
			'emulatePrepare'     => true,
			'username'           => 'root',
			'password'           => '28051989',
			'charset'            => 'utf8',
			'enableParamLogging' => true,
		],
		'errorHandler' => [
			'errorAction' => 'main/error',
		],
		'log'          => [
			'class'  => 'CLogRouter',
			'routes' => [
				[
					'class'  => 'CFileLogRoute',
					'levels' => 'error, warning, info, trace',
				],
			
			],
		],
		'viewRenderer' => [
			'class'         => 'ext.ETwigViewRenderer',
			'twigPathAlias' => 'ext.twig-renderer.lib.Twig',
			'fileExtension' => '.twig',
			'paths'         => ['__main__' => 'application.views'],
			'functions'     => [
				'count' => 'count',
				'ceil'  => 'ceil',
				'dump'  => 'var_dump',
				'round' => 'round',
				'die'   => 'die',
			],
			'globals'       => [
				'Yii' => 'Yii',
				'Nav' => 'Navigation',
			],
		],
		'cache'        => array(
			'class' => 'CFileCache',
		),
		'user'         => [
			'class'          => 'application.components.WebUser',
			'loginUrl'       => ['main/signIn'],
			'returnUrl'      => ['main/journal'],
			'allowAutoLogin' => true,
			'identityCookie' => [
				'path'   => '/',
				'domain' => (array_key_exists('HTTP_HOST', $_SERVER)? '.' . $_SERVER['HTTP_HOST']: null),
			],
			'authTimeout'    => 2592000, // 1 month in seconds
		],
		'authManager' => [
			'class'        => 'application.components.AuthManager',
			// Роль по умолчанию. Все, кто не админы, модераторы и юзеры — гости.
			'defaultRoles' => ['guest'],
		],
	],

	'params'         => [
		'adminEmail' => '',
	],
];
