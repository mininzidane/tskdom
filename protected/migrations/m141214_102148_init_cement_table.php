<?php

class m141214_102148_init_cement_table extends DbMigration {

	public function safeUp() {
		$this->createTable('driver', [
			'id'      => 'INT(5) UNSIGNED NOT NULL AUTO_INCREMENT',
			'name'    => 'VARCHAR(255) NOT NULL',
			'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);

		$this->createTable('cement', [
			'id'         => 'INT(5) UNSIGNED NOT NULL AUTO_INCREMENT',
			'date'       => 'DATE NULL DEFAULT NULL',
			'city'       => 'VARCHAR(128) NOT NULL',
			'silage'     => 'INT(2) NOT NULL DEFAULT 1',
			'outgo'      => 'FLOAT NULL DEFAULT NULL',
			'incoming'   => 'FLOAT NULL DEFAULT NULL',
			'difference' => 'FLOAT NULL DEFAULT NULL',
			'remains'    => 'FLOAT NULL DEFAULT NULL',
			'driverId'   => 'INT(5) UNSIGNED NULL DEFAULT NULL',
			'isUpdated'  => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
			'updated'    => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);
	}

	public function safeDown() {
		$this->dropTable('cement');
		$this->dropTable('driver');
	}
}
