<?php

/**
 * Class Tipper
 *
 * @author Lukin A.
 *
 * @property int    $id
 * @property string $name
 * @property int    $regNumber
 * @property int    $volume
 * @property int    $chassisCount
 * @property int    $bodyLength
 * @property int    $bodyWidth
 */
class Tipper extends ActiveRecord {

    public $name;
    public $regNumber;
    public $volume;
    public $chassisCount;
    public $bodyLength;
    public $bodyWidth;

    /**
     * @param string $className
     * @return self
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function defaultScope() {
        return [
            'order' => 't.created DESC',
        ];
    }

    public function rules() {
        return [
            ['name, regNumber', 'required'],
            ['volume, bodyLength, bodyWidth', 'numerical'],
            ['chassisCount', 'numerical', 'integerOnly' => true],
        ];
    }

    public function attributeLabels() {
        return [
            'name'         => 'Название машины',
            'regNumber'    => 'Госномер',
            'volume'       => 'Объем кузова',
            'chassisCount' => 'Количество шасси',
            'bodyLength'   => 'Длина кузова',
            'bodyWidth'    => 'Ширина кузова',
        ];
    }
}
