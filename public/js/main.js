/**
 * Created by PhpStorm.
 * User: Lukin Anton
 * Date: 20.11.2014
 * Time: 23:12
 */

/**
 * Меняет в таблице ссылку и текст для слова "выполнить" и цвет колонки
 */
function setCompletedStatusForOrderTableRow() {
	var $this = $(this);

	$this.text('Выполнена').closest('tr').attr('class', '');
	$this.wrapInner('<span class="text-muted">');
	$this.find('span').unwrap('<a>');
}

/**
 * Обновляет данные для таблицы заказов
 */
function refreshTable() {
	var $body = $('body'),
		url = $body.data('last-table-url');

	$.get(url, function (data) {
		$('.js-order-table').html(data);
	});
}

function refreshCalendar() {
	$('.js-calendar').fullCalendar('refetchEvents');
}

function changeCementRemainsValue() {
	$('.js-cement-remains').text($('.js-change-cement-remains').val());
}

$(function () {
	var $calendar = $('.js-calendar'),
		$dateTarget = $('.js-date'),
		events = [],
		$body = $('body'),
		eventsJSON = $calendar.data('events');

	var fancySettings = {
		scrolling: 'no',
		autoSize: false,
		width: 800,
		height: 'auto',
		fitToView: false
	};

	/**
	 * Создает алерт нужного цвета и прокручивает страницу наверх до него и потом скрывает алерт
	 *
	 * @param {bool} success
	 * @returns {*|jQuery}
	 */
	var getNotifier = function(success) {
		var $notifier = $('<div class="alert container" />').prependTo('body');

		if (parseInt(success)) {
			$notifier.text('Удачно').addClass('alert-success');
		} else {
			$notifier.text('Ошибка').addClass('alert-danger');
		}

		$('html, body').animate({ scrollTop: 0 }, "slow", function() {
			setTimeout(function() {
				$notifier.fadeOut();
			}, 2000);
		});
		return $notifier;
	};

	/**
	 * Вызов окна "Вы уверены?" на нужных действиях
	 *
	 * @returns {*}
	 */
	var confirmation = function() {
		return confirm('Вы уверены?');
	};

	/**
	 * Показывает фансибокс окно подтверждения
	 *
	 * @param start
	 * @param end
	 */
	var calendarDialog = function(start, end) {
		if ($calendar.is('.js-calendar-no-select')) {
			calendarSetEvent(start, end, false);
			return;
		}

		var timeLabels = '', dayEvents = $body.data('dayEvents');

		if (dayEvents && dayEvents.length) {
			timeLabels = '<div>Занятое время:</div><p>';
			$.each(dayEvents.sort(), function (i, item) {
				timeLabels += '<span class="label label-danger">' + item + '</span> ';
			});
			timeLabels += '</p>';
		}

		var template =
			'<div>' +
				timeLabels +
				'<p>Введите время (формат ЧЧММ):</p>' +
				'<div class="form-group">' +
					'<input class="form-control js-calendar-time">' +
				'</div>' +
				'<div class="form-group">' +
					'<button class="btn btn-success js-calendar-time-ok">Ok</button>&nbsp;' +
					'<button class="btn btn-danger js-calendar-time-cancel">Отмена</button>' +
				'</div>' +
			'</div>';

		$.fancybox(template, $.extend(fancySettings, {
			width: 400
		}));

		$body
			.off('click', '.js-calendar-time-ok')
			.on('click', '.js-calendar-time-ok', function () {
				calendarSetEvent(start, end);
				return false;
			})
			.off('click', '.js-calendar-time-cancel')
			.on('click', '.js-calendar-time-cancel', function() {
				$.fancybox.close();
				return false;
			});
	};

	/**
	 * Устанавливает значение времени
	 *
	 * @param start
	 * @param end
	 * @param [selectable]
	 */
	var calendarSetEvent = function(start, end, selectable) {
		selectable = selectable !== false;

		var eventData,
			startTime = $('.js-calendar-time').val();

		if (startTime) {
			var timeSeparatorPosition = startTime.length === 3? 1: 2;
			startTime = [startTime.slice(0, timeSeparatorPosition), ':', startTime.slice(timeSeparatorPosition)].join('');
		}

		if (selectable === false || startTime) {
			if (selectable) {
				eventData = {
					title: 'Новая заявка',
					start: start.time(startTime),
					end: end
				};
				$calendar.fullCalendar('renderEvent', eventData, true); // stick? = true
				$dateTarget.val(start.format());
			}

			var url = $calendar.data('orders-url') + '?date=' + start.format();
			$.get(url, function (data) {
				$body.data('last-table-url', url);
				$('.js-order-table').html(data);
			});
		}
		$calendar.fullCalendar('unselect');
		$.fancybox.close();
	};

	if ($calendar.length) {
		if (eventsJSON) {
			$.each(eventsJSON, function (i, item) {
				events.push({
					title: item.title,
					start: item.start,
					end: item.end
				});
			});
		}

		var settings = {
			header: {
				left: 'title',
				center: ''
				//right: ''
			},
			//editable: true,
			selectable: true,
			allDayDefault: false,
			timeFormat: 'HH:mm',
			select: function (start, end) {
				calendarDialog(start, end);
			},
			dayClick: function (date) {
				var eventTimes = [];

				$calendar.fullCalendar('clientEvents', function (event) {
					if (event.start.format('YYYY-MM-DD') == date.format('YYYY-MM-DD')) {
						eventTimes.push(event.start.format('HH:mm'));
					}
				});

				$body.data('day-events', eventTimes);
				$('.fc-row .fc-day:not(.fc-today)').css('background-color', 'transparent');
				$(this).css('background-color', '#f0ad4e');
			}
		};

		if (events.length) {
			settings.events = events;
		}

		var dataSource;
		if (dataSource = $calendar.data('event-source')) {
			settings.eventSources = [dataSource];

			// обновляем ивенты каждые 30 сек
			setInterval(function() {
				$calendar.fullCalendar('refetchEvents');
			}, 30000);
		}

		$calendar.fullCalendar(settings);
	}

	var $groupButtons = $('.js-select-group').on('click', function() {
		var $this = $(this),
			id = $this.attr('href') || $this.data('id');

		$groupButtons.removeClass('active');
		$this.addClass('active');
		$('.js-select-group-items').hide();
		$(id).show();
		if ($selectGroupItems && $selectGroupItems.find('button')) {
			$selectGroupItems.find('button').removeClass('active');
		}

		// -- скрываем элементы при выборе инертных материалов
		var $itemsToHide = $('#additive, .js-shwing, .js-plus-starting, .js-plus-starting-value, .js-plus-bag, .js-plus-bag-value, .js-extra');
		if ($this.data('id') == 'inert-materials') {
			$itemsToHide.hide();
		} else {
			$itemsToHide.show();
		}
		// -- -- -- --

		return false;
	});
	$groupButtons.filter('.active').trigger('click');

	var $selectGroupItems = $('.js-select-group-items').on('click', 'button:not(.js-calculate)', function() {
		var $this = $(this);

		$selectGroupItems.find('button').removeClass('active');
		$this.addClass('active');
		return false;
	});

	$('.js-select-button').on('click', 'button', function() {
		var $this = $(this),
			$selectButton = $this.closest('.js-select-button'),
			$buttons = $selectButton.find('button');

		if ($buttons.length > 1) {
			$buttons.removeClass('active');
			$this.addClass('active');
		} else {
			$this.toggleClass('active');
		}
		return false;
	});

	// address adding
	var $addAddressParent = $('.js-add-address');
	$body.on('click', '.js-add-address .js-add-address-trigger', function() {
		var $this = $(this),
			$addAddress = $this.closest('.row');

		$addAddress.clone().insertAfter($addAddress).find('input').val('');
		return false;
	});

	// customer auto complete
	var $customerComplete = $('.js-customer-complete');
	if ($customerComplete.length) {
		$customerComplete.autocomplete({
			lookup: $customerComplete.data('customers'),
			onSelect: function (suggestion) {
				// clear before any event trigger
				$addAddressParent.find('.row:not(:first)').remove();

				$('#Customer_id').val(suggestion.data.id);
				$('#Customer_phone').val(suggestion.data.phone);
				$('#Customer_comment').val(suggestion.data.comment);
				$.each(suggestion.data.addresses, function(i, item) {
					$addAddressParent.find('.js-add-address-trigger').eq(i).trigger('click');
					$addAddressParent.find(':text').eq(i).val(item);
				});
			}
		});
	}

	// concrete order before submit handler
	$('.js-concrete-order').on('submit', function() {
		var $this = $(this),
			recipe = {};

		$this.find('.active[data-id]').each(function() {
			var $this = $(this),
				value = $this.data('id');

			if (isNaN(parseInt(value))) { // if string in data-id
				recipe.type = value;
			} else if ($this.parent().is('#additive')) { // if this is additive
				recipe.additive = value;
			} else if ($this.is('.js-plus-starting')) {
				value = $('.js-plus-starting-value').val() || value;
				recipe.starting = value;
				$('#ConcreteOrder_volume').val(function(i, val) {
					return (parseFloat(val) + parseFloat(value));
				});
			} else if ($this.is('.js-shwing')) {
				recipe.shwing = value;
			} else if ($this.is('.js-plus-bag')) {
				value = $('.js-plus-bag-value').val() || 0;
				recipe.bag = value;
			} else {
				recipe.value = value;
			}
		});

		$this.find('.js-extra').each(function() {
			var $this = $(this),
				type = $this.data('extra'),
				value = $this.val();

			if (value != '') {
				recipe[type] = value;
			}
		});

		$('#ConcreteOrder_recipe').val(JSON.stringify(recipe));

		if ($this.find('.js-select-group-items').length && !$this.find('.js-select-group-items:visible .active').length) {
			alert('Выберите марку бетона!');
			return false;
		}

		return true;
	});

	// spec order before submit handler
	$('.js-spec-order').on('submit', function() {
		var $this = $(this);

		$this.find('.active[data-id]').each(function () {
			var $this = $(this),
				value = $this.data('id');

			$('#SpecOrder_specModel').val(value);
		});
		return true;
	});

	// alert disappear
	setTimeout(function() {
		$('.js-alert-disappear').fadeOut();
	}, 3000);

	// links with ajax
	$body.on('click', '.js-ajax-link', function() {
		if (!confirmation()) {
			return false;
		}

		var $this = $(this);

		$.fancybox.showLoading();
		$.ajax({
			url: $this.attr('href') || $this.data('url'),
			success: function(data) {
				getNotifier(data);

				if (parseInt(data)) {
					var func = $this.data('callback');

					if (window[func]) {
						window[func].call($this);
					}
				}
			},
			complete: function() {
				$.fancybox.hideLoading();
			}
		});
		return false;
	});

	// ajax via fancybox
	$body.on('click', '.js-fancy-ajax', function() {
		var $this = $(this);

		$.fancybox({
			href: $this.attr('href') || $this.data('href')
		}, $.extend({type: 'ajax'}, fancySettings));
		return false;
	});

	// add additional params to form url
	$body.on('click', 'button.js-ajax-submit', function() {
		var $this = $(this),
			name;

		if (name = $this.attr('name')) {
			$this.after('<input type="hidden" name="' + name + '" value="1">');
		}
	});

	// submit form via ajax
	$body.on('submit', '.js-ajax-submit', function() {
		if (!confirmation()) {
			return false;
		}

		var $this = $(this),
			$form = $this.closest('form'),
			data = $form.serialize();

		$.fancybox.showLoading();

		// если кнопка удаления
		if ($this.is('[name=delete')) {
			data += '&delete=1';
		}

		// если восстановления
		if ($this.is('[name=recover')) {
			data += '&recover=1';
		}

		$.ajax({
			url: $form.attr('action'),
			data: data,
			type: $form.attr('method') || 'get',
			success: function(data) {
				getNotifier(data);

				$.fancybox.close();

				var func = $this.data('callback');

				if (window[func]) {
					window[func].call($this);
				}
			},
			complete: function() {
				$.fancybox.hideLoading();
			}
		});
		return false;
	});

	// customer edit
	$body.on('click', '.js-customer-edit', function() {
		var $this = $(this),
			$select = $('.js-customer-select'),
			isAddMode = $this.data('mode') == 'add',
			value = isAddMode? 0: $select.find(':selected').val();

		if (!isAddMode && !parseInt(value)) {
			alert('Выберите заказчика');
			return false;
		}

		$.fancybox({
			href: $select.data('url') + '?customerId=' + value
		}, $.extend({type: 'ajax'}, fancySettings));
		return false;
	});

	// customer address deleting
	$body.on('click', '.js-delete-row', function() {
		if (!confirmation()) {
			return false;
		}

		var $this = $(this);

		$.fancybox.showLoading();
		$.ajax({
			url: $this.data('url') || $this.data('href'),
			success: function (data) {
				if (parseInt(data)) {
					$this.closest('.row, tr').fadeOut(function () {
						$(this).remove();
					});
				}
			},
			complete: function () {
				$.fancybox.hideLoading();
			}
		});

		return false;
	});

	// clone table form row
	$body.on('click', '.js-add-clone-row', function() {
		var $this = $(this),
			$row = $this.closest('tr');

		$row.clone().insertAfter($row);
		return false;
	});

	// check table form row
	$body.on('click', '.js-check-row', function() {
		var $this = $(this),
			$tr = $this.closest('tr');

		$tr.toggleClass('success');
	});

	// show fancybox from a target html block
	$body.on('click', '.js-fancy-html-block', function() {
		var $this = $(this),
			func = $this.data('callback'),
			$target = $($this.data('target'));

		$.fancybox($target, $.extend(fancySettings, {
			afterShow: function() {
				if (window[func]) {
					window[func].call($this);
				}
			}
		}));
		return false;
	});

	// silage toggle
	$body.on('change', '.js-select-toggle', function() {
		var $this = $(this),
			id = $this.val();

		$('.js-select-toggle-content').hide();
		if (id) {
			$('[data-id = ' + id + ']').show();
		}
	});

	// select click
	$body.on('click', '.js-url-click', function() {
		var $this = $(this),
			$target = $($this.data('target'));

		window.open($target.val(), '_blank');
		return false;
	});

	// изменение остатков
	$body.on('keyup', '.js-change-cement-remains', function() {
		var $this = $(this),
			$button = $this.closest('.row').find('.js-ajax-link'),
			url = $button.data('url').split('?')[0];

		url += '?remains=' + $this.val();
		$button.data('url', url);
	});

	// меняем запятую на точку
	$body.on('change', '.js-replace-comma', function() {
		var $this = $(this);
		$this.val($this.val().replace(',', '.'));
	});

	// универсальный метод для клонирования элементов
	$body.on('click', '.js-clone-element', function() {
		var $this = $(this),
			$target = $($this.data('target')).last();

		var $clone = $target.clone().insertAfter($target);
		$clone.find(':text').val('');
		return false;
	});

	// считаем, сколько отгружено бетона по миксерам
	$body.on('change', '.js-mixer-boxes', function() {
		var $this = $(this),
			concreteTotal = 0;

		$this.find('.js-mixer-quantity').each(function() {
			var val = $(this).val().replace(',', '.');
			if (val) {
				concreteTotal += parseFloat(val);
			}
		});

		$this.find('.js-count-mixers').text(concreteTotal);
	});

	// простой колбэк удаления строки (.row или строки таблицы)
	$body.on('click', '.js-simple-delete-row', function() {
		$(this).closest('.row, tr').fadeOut(function () {
			$(this).remove();
		});
		return false;
	});
});