<?php

return array(
	// frontend actions
	'/'                        => 'main/journal',
	'/concrete-order'          => 'main/index',
	'/spec-order/'             => 'main/specOrder',
	'/editSpecModels/'         => 'main/editSpecModels',
	'/editSpecTypes/'          => 'main/editSpecTypes',
	'/logistic/'               => 'main/logistic',
	'/logistic/<id:\d+>'       => 'main/editLogistic',
	'/calculator/'             => 'main/calculator',
	'/cement/'                 => 'main/cement',

	'/sign-in/'                => 'main/signIn',
	'/sign-out/'               => 'main/signOut',
	'/error/'                  => 'main/error',

	// users actions
	'/users/'                  => 'user/index',
	'/users/edit/<id:\d+>/'    => 'user/edit',
	'/users/create/'           => 'user/create',

	// ajax actions
	'/nearestConcreteOrders/'  => 'ajax/getNearestConcreteOrders',
	'/nearestSpecOrders/'      => 'ajax/getNearestSpecOrders',
	'/nearestJournal/'         => 'ajax/getNearestJournal',
	'/orderStatus/'            => 'ajax/setOrderStatus',
	'/changeOrder/'            => 'ajax/changeOrder',
	'/customerEdit/'           => 'ajax/customerEdit',
	'/deleteAddress/'          => 'ajax/deleteAddress',
	'/deleteModel/'            => 'ajax/deleteModel',
	'/changeModelAttribute/'   => 'ajax/toggleModelAttribute',
	'/getCementStat/'          => 'ajax/getCementStat',
	'/getCementExcel/'         => 'ajax/getCementExcel',
	'/getCementExcelByPeriod/' => 'ajax/getCementExcelByPeriod',
	'/getOrdersExcel/'         => 'ajax/getOrdersExcel',
	'/changeCementRemains/'    => 'ajax/changeCementRemains',
	'/editMixers/'             => 'ajax/editMixers',
	'/ajax/<action>/'          => 'ajax/<action>',
	'/<controller>/<action>/'  => '<controller>/<action>',
);
