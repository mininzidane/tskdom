<?php

class m150120_150155_init_concrete_mixer_table extends DbMigration {

	public function safeUp() {
		$this->createTable('concrete_mixer', [
			'id'        => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT',
			'mixerInfo' => 'TEXT NULL DEFAULT NULL',
			'orderId'   => 'INT(5) UNSIGNED NULL DEFAULT NULL',
			'created'   => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);
	}

	public function safeDown() {
		$this->dropTable('concrete_mixer');
	}
}
