<?php

/**
 * Class WebUser
 *
 * @author Lukin A.
 *
 * @property string $role
 */
class WebUser extends CWebUser {
	const ROLE_USER                = 'user';
	const ROLE_MANAGER             = 'manager';
	const ROLE_ADMIN               = 'admin';
	const ROLE_CEMENT_MANAGER      = 'cementmanager'; // только для объединения менеджеров по цементу Вл и Хаб
	const ROLE_LOGISTICIAN         = 'logistician';
	const ROLE_KHAB_CEMENT_MANAGER = 'KhabCementManager';

	public static $roles = [
		self::ROLE_USER,
		self::ROLE_MANAGER,
		self::ROLE_ADMIN,
		self::ROLE_LOGISTICIAN,
		self::ROLE_KHAB_CEMENT_MANAGER,
	];

	/** @var User */
	public $model;

	protected function getModel() {
		if (!$this->isGuest && $this->model === null) {
			$this->model = User::model()->findByPk($this->id);
		}
		return $this->model;
	}

	public function getRole() {
		if ($model = $this->getModel()) {
			return $model->role;
		}
		return null;
	}
}
