<?php

class m160516_055442_add_tipper_table extends DbMigration {

    public function safeUp() {
        $this->createTable('tipper', [
            'id'           => 'INT(5) UNSIGNED NOT NULL AUTO_INCREMENT',
            'name'         => 'VARCHAR(64) NOT NULL',
            'regNumber'    => 'VARCHAR(16) NULL DEFAULT NULL',
            'volume'       => 'INT(9) NULL DEFAULT NULL',
            'chassisCount' => 'INT(2) NULL DEFAULT NULL',
            'bodyLength'   => 'INT(9) NULL DEFAULT NULL',
            'bodyWidth'    => 'INT(9) NULL DEFAULT NULL',
            'created'      => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'PRIMARY KEY (id)',
        ]);
    }

    public function safeDown() {
        $this->dropTable('tipper');
    }
}