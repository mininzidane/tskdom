<?php

/**
 * Class ConcreteOrder
 *
 * @author Lukin A.
 *
 * @property int        $id
 * @property string     $recipe
 * @property int        $volume
 * @property string     $date
 * @property string     $comment
 * @property int        $customerId
 * @property int        $status
 *
 * @property Customer        $customer
 * @property ConcreteMixer   $mixer
 */
class ConcreteOrder extends ActiveRecord {
	public $comment;

	/** Текстовый идентификатор типа */
	const ORDER_TYPE = 'concrete';

	// состав рецептов
	const TYPE     = 'type'; // Тип бетона
	const VALUE    = 'value'; // Значение
	const ADDITIVE = 'additive'; // Добавки
	const STARTING = 'starting'; // Пусковая
	const SHWING   = 'shwing'; // Под швинг
	const BAG      = 'bag'; // Мешочек

	// extra
	const EXTRA_W = 'W';
	const EXTRA_F = 'F';
	const EXTRA_P = 'P';

	public static $recipeConsist = [
		self::TYPE,
		self::VALUE,
		self::ADDITIVE,
		self::STARTING,
		self::SHWING,
		self::BAG,
		self::EXTRA_W,
		self::EXTRA_F,
		self::EXTRA_P,
	];

	protected function afterSave() {
		$this->saveAddressToOrder();

		parent::afterSave();
	}

	/**
	 * @param string $className
	 * @return ConcreteOrder
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function getRecipes() {
		return [
			'concrete'        => [
				'label'  => 'Бетон',
				'values' => [
					'М100',
					'М150',
					'М200',
					'М250',
					'М300',
					'М350',
					'М400',
					'М450',
					'М500',
					'?',
				]
			],
			'solute'          => [
				'label'  => 'Раствор',
				'values' => [
					'М75',
					'М100',
					'М150',
					'М200',
					'?',
				],
			],
			'polystyrene'     => [
				'label'  => 'Полистиролбетон',
				'values' => [
					'Д200',
					'Д300',
					'Д400',
					'Д600',
					'Д800',
				],
			],
			'waterproofing'   => [
				'label'  => 'Гидроизоляционный бетон',
				'values' => [
					'В25',
				],
			],
			'legodom'         => [
				'label'  => 'Бетон Легодом',
				'values' => [
					'В15',
					'В25',
				],
			],
			'inert-materials' => [
				'label'  => 'Инертные материалы',
				'values' => [
					'Песок',
					'Щебень 5-10',
					'Щебень 10-20',
				]
			],
		];
	}

	public function getAdditives() {
		return [
			'Без добавок',
			'до +5',
			'до -5',
			'до -10',
			'до -15',
			'до -20',
			'до -25',
		];
	}

	public function getExtraParams() {
		return [
			self::EXTRA_W => [
				'W2',
				'W4',
				'W6',
				'W8',
				'W10',
				'W12',
				'W14',
			],
			self::EXTRA_F => [
				'F75',
				'F100',
				'F150',
				'F200',
				'F250',
				'F300',
				'F400',
			],
			self::EXTRA_P => [
				'П1',
				'П2',
				'П3',
				'П4',
				'П5',
			],
		];
	}

	/**
	 * Получает название доп. параметров бетона по типу и значению
	 *
	 * @author Lukin A.
	 *
	 * @param $type
	 * @param $value
	 * @return mixed
	 */
	public function getExtraLabel($type, $value) {
		return $this->getExtraParams()[$type][$value];
	}

	public function tableName() {
		return 'concrete_order';
	}

	public function scopes() {
		return CMap::mergeArray(parent::scopes(), [

		]);
	}

	public function relations() {
		return [
			'customer' => [self::HAS_ONE, 'Customer', ['id' => 'customerId']],
			'mixer'    => [self::HAS_ONE, 'ConcreteMixer', 'orderId'],
		];
	}

	public function rules() {
		return [
			['recipe, volume, date, customerId', 'required'],
			['customerId', 'numerical'],
			['comment', 'safe'],
		];
	}

	public function attributeLabels() {
		return [
			'date'    => 'Дата',
			'recipe'  => 'Рецепт',
			'volume'  => 'Объем',
			'comment' => 'Комментарий',
		];
	}

	/**
	 * Вернет значение конкретной составляющей рецепта бетона, либо массив со всеми составляющими
	 *
	 * @author Lukin A.
	 *
	 * @param null|string $attribute Составляющая бетона
	 * @return array|string
	 */
	public function getRecipeConsist($attribute = null) {
		$allRecipes = $this->getRecipes();
		$recipe = $this->parseRecipe();

		@$type = $allRecipes[$recipe['type']]['label'];
		@$value = $allRecipes[$recipe['type']]['values'][$recipe['value']];
		@$additive = $this->getAdditives()[$recipe['additive']];
		@$starting = $recipe[self::STARTING];
		@$shwing = $recipe[self::SHWING];
		@$bag = $recipe[self::BAG];
		@$extraW = $this->getExtraLabel(self::EXTRA_W, $recipe[self::EXTRA_W]);
		@$extraF = $this->getExtraLabel(self::EXTRA_F, $recipe[self::EXTRA_F]);;
		@$extraP = $this->getExtraLabel(self::EXTRA_P, $recipe[self::EXTRA_P]);;

		$recipe = [
			self::TYPE     => $type,
			self::VALUE    => $value,
			self::ADDITIVE => $additive,
			self::STARTING => $starting,
			self::SHWING   => $shwing,
			self::BAG      => $bag,
			self::EXTRA_W  => $extraW,
			self::EXTRA_F  => $extraF,
			self::EXTRA_P  => $extraP,
		];

		if ($attribute !== null && array_key_exists($attribute, $recipe)) {
			return $recipe[$attribute];
		}

		return $recipe;
	}

	/**
	 * Разбирает и возвращает значение рецепта как JSON. В случае неудачи вернет пустой массив
	 *
	 * @author Lukin A.
	 *
	 * @return array [type => '', value => '', additive => '']
	 */
	public function parseRecipe() {
		$recipe = json_decode($this->recipe);
		if ($recipe === null || !property_exists($recipe, 'type')) {
			return [];
		}

		$ret = [
			'type' => $recipe->type,
		];

		foreach (self::$recipeConsist as $recipeItem) {
			if (property_exists($recipe, $recipeItem)) {
				$ret[$recipeItem] = $recipe->$recipeItem;
			}
		}

		return $ret;
	}

	/**
	 * Собирает в формате JSON рецепт из составляющих и пишет это в recipe
	 *
	 * @author Lukin A.
	 *
	 * @param string   $type
	 * @param int      $value
	 * @param int|null $additive
	 * @param int|null $starting
	 * @param int|null $shwing
	 * @param int|null $bag
	 */
	public function prepareRecipe($type, $value, $additive = null, $starting = null, $shwing = null, $bag = null, $extraW = null, $extraF = null, $extraP = null) {
		$recipe = [
			self::TYPE  => $type,
			self::VALUE => $value,
		];
		if ($additive) {
			$recipe[self::ADDITIVE] = $additive;
		}
		if ($starting) {
			$recipe[self::STARTING] = $starting;
		}
		if ($shwing) {
			$recipe[self::SHWING] = $shwing;
		}
		if ($bag) {
			$recipe[self::BAG] = $bag;
		}
		if ($extraW) {
			$recipe[self::EXTRA_W] = $extraW;
		}
		if ($extraF) {
			$recipe[self::EXTRA_F] = $extraF;
		}
		if ($extraP) {
			$recipe[self::EXTRA_P] = $extraP;
		}

		$this->recipe = json_encode($recipe);
	}

	/**
	 * Получает общую сумму заявки
	 *
	 * @author lukin.a
	 *
	 * @return int
	 */
	public function getMixersTotalVolume() {
		$totalVolume = 0;
		if ($this->mixer) {
			foreach ($this->mixer->getMixers() as $mixerInfo) {
				$totalVolume += current($mixerInfo);
			}

			return $totalVolume;
		}

		return $totalVolume;
	}
	public function defaultScope() {
  return [
   'order' => 't.date DESC, t.customerId ASC'
  ];
 }
}
