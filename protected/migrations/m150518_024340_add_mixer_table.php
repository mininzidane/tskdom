<?php

class m150518_024340_add_mixer_table extends DbMigration {

	public function safeUp() {
		$this->createTable('mixer', [
			'id'      => 'INT(5) UNSIGNED NOT NULL AUTO_INCREMENT',
			'number'  => 'VARCHAR(64) NOT NULL',
			'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);
	}

	public function safeDown() {
		$this->dropTable('mixer');
	}
}
