<?php

/**
 * Модель спецтехники
 *
 * @author Lukin A.
 *
 * @property int      $id
 * @property string   $title
 *
 * @property SpecType $specType
 */
class SpecModel extends ActiveRecord {

	/**
	 * @param string $className
	 * @return SpecModel
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'spec_model';
	}

	public function relations() {
		return [
			'specType' => [self::BELONGS_TO, 'SpecType', 'specTypeId'],
		];
	}

	public function rules() {
		return [
			['title', 'required'],
			['specTypeId', 'safe'],
		];
	}

	public function attributeLabels() {
		return [
			'title'   => 'Название модели',
			'title[]' => 'Название модели',
		];
	}
}
