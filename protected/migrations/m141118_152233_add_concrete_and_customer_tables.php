<?php

class m141118_152233_add_concrete_and_customer_tables extends DbMigration {

	public function safeUp() {
		$this->createTable('concrete_order', [
			'id'         => 'INT(5) UNSIGNED NOT NULL AUTO_INCREMENT',
			'recipe'     => 'VARCHAR(255) NULL DEFAULT NULL',
			'volume'     => 'INT(11) UNSIGNED NULL DEFAULT NULL',
			'date'       => 'TIMESTAMP NULL DEFAULT NULL',
			'customerId' => 'INT(5) UNSIGNED NULL DEFAULT NULL',
			'created'    => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);

		$this->createTable('customer', [
			'id'        => 'INT(5) UNSIGNED NOT NULL AUTO_INCREMENT',
			'title'     => 'VARCHAR(255) NOT NULL',
			'phone'     => 'VARCHAR(255) NULL DEFAULT NULL',
			'comment'   => 'TEXT NULL DEFAULT NULL',
			'created'   => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);

		$this->createTable('customer_address', [
			'id'         => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT',
			'address'    => 'VARCHAR(255) NOT NULL',
			'customerId' => 'INT(5) UNSIGNED NOT NULL',
			'created'    => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);

		$this->addForeignKey('concrete_order_customer', 'concrete_order', 'customerId', 'customer', 'id');
		$this->addForeignKey('customer_address_customer', 'customer_address', 'customerId', 'customer', 'id');
	}

	public function safeDown() {
		$this->dropForeignKey('customer_address_customer', 'customer_address');
		$this->dropForeignKey('concrete_order_customer', 'concrete_order');
		$this->dropTable('customer_address');
		$this->dropTable('customer');
		$this->dropTable('concrete_order');
	}
}
