<?php

/**
 * Class ConcreteOrder
 *
 * @author Lukin A.
 *
 * @property int        $id
 * @property int        $specModel
 * @property string     $date
 * @property string     $comment
 * @property int        $customerId
 * @property int        $status
 *
 * @property Customer   $customer
 * @property SpecModel  $spec
 */
class SpecOrder extends ActiveRecord {
	public $comment;

	const ORDER_TYPE = 'spec';

	protected function afterSave() {
		$this->saveAddressToOrder();

		parent::afterSave();
	}

	/**
	 * @param string $className
	 * @return ConcreteOrder
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'spec_order';
	}

	public function scopes() {
		return CMap::mergeArray(parent::scopes(), [

		]);
	}

	public function relations() {
		return [
			'customer' => [self::HAS_ONE, 'Customer', ['id' => 'customerId']],
			'spec'     => [self::HAS_ONE, 'SpecModel', ['id' => 'specModel']],
		];
	}

	public function rules() {
		return [
			['date, customerId, specModel', 'required'],
			['comment', 'safe'],
		];
	}

	public function attributeLabels() {
		return [
			'date'      => 'Дата',
			'specModel' => 'Модель спецтехники',
			'comment'   => 'Комментарий',
		];
	}
	public function defaultScope() {
  return [
   'order' => 't.date DESC, t.customerId ASC'
  ];
 }
}
