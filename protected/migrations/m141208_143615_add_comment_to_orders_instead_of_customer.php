<?php

class m141208_143615_add_comment_to_orders_instead_of_customer extends DbMigration {

	public function safeUp() {
		$this->addColumn('concrete_order', 'comment', 'TEXT NULL DEFAULT NULL AFTER date');
		$this->addColumn('spec_order', 'comment', 'TEXT NULL DEFAULT NULL AFTER date');
		$this->dropColumn('customer', 'comment');
	}

	public function safeDown() {
		$this->addColumn('customer', 'comment', 'TEXT NULL DEFAULT NULL AFTER date');
		$this->dropColumn('spec_order', 'comment');
		$this->dropColumn('concrete_order', 'comment');
	}
}
