<?php

/**
 * @property string $password
 */
class User extends ActiveRecord {
	/** @var int */
	public $id;
	/** @var string */
	public $username;
	/** @var string */
	public $password;
	/** @var string */
	protected $salt;
	/** @var string */
	public $email;
	/** @var string */
	public $description;
	/** @var string */
	public $role;
	/** @var string */
	public $realname;

	/**
	 * @var string
	 * @transient
	 */
	protected $confirmPassword;
	/**
	 * @var boolean
	 * @transient
	 */
	protected $needEncodePassword = false;

	/**
	 * @param string $className
	 * @return User
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/** @return string */
	public function tableName() {
		return 'user';
	}

	/** @return array */
	public function rules() {
		return array(
			array('username, email, password', 'required'),
			array('username, realname, email', 'length', 'max' => 100),
			array('email', 'email'),
			array('username, email', 'unique'),
			array('confirmPassword', 'compare', 'compareAttribute' => 'password'),
			array('password, confirmPassword', 'unsafe'),

			array('username, email, realname, description', 'safe'),
			array('id, username, email', 'safe', 'on' => 'search'),
			array('role', 'in', 'range' => WebUser::$roles),
		);
	}

	/** @return array */
	public function attributeLabels() {
		return array(
			'id'              => 'ID',
			'username'        => 'Имя пользователя',
			'realname'        => 'Реальное имя',
			'password'        => 'Пароль',
			'confirmPassword' => 'Повтор пароля',
			'salt'            => 'Соль пароля',
			'email'           => 'Email',
			'description'     => 'Описание',
		);
	}

	/**
	 * @param string $password
	 * @param bool   $encode
	 * @return void
	 */
	public function setPassword($password, $encode = true) {
		$this->password = $password;
		$this->needEncodePassword = $encode;
	}

	/**
	 * @return string
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * @param string $password
	 * @return void
	 */
	public function setConfirmPassword($password) {
		$this->confirmPassword = $password;
	}

	/**
	 * @return string
	 */
	public function getConfirmPassword() {
		return $this->confirmPassword;
	}

	/**
	 * @return string
	 */
	private function _getSalt() {
		if (!$this->salt) {
			$this->salt = $this->_generateSalt();
		}
		return $this->salt;
	}

	/**
	 * Checks if the given password is correct.
	 * @param string $password the password to be validated
	 * @return boolean whether the password is valid
	 */
	public function validatePassword($password) {
		return $this->_hashPassword($password, $this->_getSalt()) === $this->password;
	}

	/**
	 * Generates the password hash.
	 * @param string $password
	 * @param string $salt
	 * @return string hash
	 */
	private function _hashPassword($password, $salt) {
		return md5($salt . $password);
	}

	protected function beforeSave() {
		if ($this->needEncodePassword || !$this->salt) {
			$this->password = $this->_hashPassword($this->password, $this->_getSalt());
		}
		return parent::beforeSave();
	}

	protected function afterFind() {
		$this->confirmPassword = $this->password;
		parent::afterFind();
	}


	/**
	 * Generates a salt that can be used to generate a password hash.
	 * @return string the salt
	 */
	private function _generateSalt() {
		return uniqid('', true);
	}

	/**
	 * Sets hashed and salted password
	 * @param string $unSafePassword
	 * @return self
	 */
	public function setSafePassword($unSafePassword) {
		$this->password = $this->_hashPassword($unSafePassword, $this->_getSalt());
		$this->confirmPassword = $this->password;
		return $this;
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('username', $this->username, true);
		$criteria->compare('email', $this->email, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
