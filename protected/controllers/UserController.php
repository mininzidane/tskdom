<?php

class UserController extends BaseController {
	protected static $saveNotifier   = 'User info has been updated successfully';
	protected static $createNotifier = 'User has been created successfully';
	protected static $deleteNotifier = 'User has been deleted successfully';

	/**
	 * @return array action filters
	 */
	public function filters() {
		return ['accessControl'];
	}

	// admin actions
	public function accessRules() {
		return [
			['allow', 'roles' => [WebUser::ROLE_ADMIN]],
			['deny', 'users' => ['*']],
		];
	}

	public function actionIndex() {
		$model = new User('search');

		if (isset($_GET['User'])) {
			$model->attributes = $_GET['User'];
		}

		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new User();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (Yii::app()->request->isPostRequest) {
			$post = Yii::app()->request->getPost(get_class($model));
			$model->attributes = $post;

			if (isset($post['password']) && $post['password']) {
				$model->password = $post['password'];
			}
			if (isset($post['confirmPassword']) && $post['confirmPassword']) {
				$model->confirmPassword = $post['confirmPassword'];
			}

			if ($model->save()) {
				Yii::app()->user->setFlash($this->action->id, self::$createNotifier);
				$this->redirect(['index']);
			}
		}

		$this->render('create', array(
			'model'   => $model,
			'isAdmin' => Yii::app()->user->checkAccess('admin'),
			'flash'   => Yii::app()->user->getFlash('form-action'),
		));
	}

	public function actionEdit($id) {
		$model = $this->loadModel($id);

		if (Yii::app()->request->isPostRequest && isset($_POST[get_class($model)])) {
			$model->attributes = $_POST[get_class($model)];

			if (isset($_POST[get_class($model)]['password']) && $_POST[get_class($model)]['password']) {
				$model->password = $_POST[get_class($model)]['password'];
			}
			if (isset($_POST[get_class($model)]['confirmPassword']) && $_POST[get_class($model)]['confirmPassword']) {
				$model->confirmPassword = $_POST[get_class($model)]['confirmPassword'];
			}

			if ($model->save()) {
				Yii::app()->user->setFlash('form-action', self::$saveNotifier);
				$this->redirect(array('edit', 'id' => $model->id));
			}
		}

		$this->render('edit', array(
			'model'   => $model,
			'isAdmin' => Yii::app()->user->checkAccess('admin'),
			'flash'   => Yii::app()->user->getFlash('form-action'),
		));
	}

	/**
	 * @param int $id
	 * @return User
	 * @throws CHttpException
	 */
	public function loadModel($id) {
		$model = User::model()->all()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}
}
