<?php

/**
 * Class Cement
 *
 * @author Lukin A.
 *
 * @property int    $id
 * @property string $name
 */
class Driver extends ActiveRecord {

	/**
	 * @param string $className
	 * @return Cement
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return [
			['name', 'required'],
		];
	}

public function defaultScope()    { 
        return array(
           'order' => 'name',
        );      
 
    }

	public function attributeLabels() {
		return [
			'name' => 'Водитель',
		];
	}
}
