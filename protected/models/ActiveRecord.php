<?php

/**
 * Class ActiveRecord
 *
 * @author Lukin A.
 *
 * @method $this active()
 *
 * @method $this with() For type hinting
 * @method $this findByPk($pk) For type hinting
 * @method $this findByAttributes(array $attributes) For type hinting
 */
class ActiveRecord extends CActiveRecord {

	// статусы заказа
	const STATUS_IN_PROGRESS = 0;
	const STATUS_COMPLETED   = 1;
	const STATUS_CANCELLED   = 2;

	const NEAREST_ORDERS_HOURS = 2;

	/** @var int Define how many days up orders info needed */
	const EXPORT_ORDERS_DAYS_FORWARD = 2;

	protected $alias;

	public static $statusLabels = [
		self::STATUS_IN_PROGRESS => 'В работе',
		self::STATUS_COMPLETED   => 'Выполнен',
		self::STATUS_CANCELLED   => 'Отменен',
	];

	public function getStatusLabel($status) {
		if (array_key_exists($status, self::$statusLabels)) {
			return self::$statusLabels[$status];
		}

		return null;
	}

	/**
	 * После сохранения заявки (бетон, спецтехника), сохраняем привязку адреса к этой заявке
	 *
	 * @author Lukin A.
	 */
	protected function saveAddressToOrder() {
		$customerParams = Yii::app()->request->getPost('Customer');
		$customerAddressParams = Yii::app()->request->getPost('CustomerAddress');

		if (!in_array(null, [$customerParams, $customerAddressParams], true)) {
			foreach ($customerAddressParams['address'] as $addressTitle) {
				if (!$addressTitle) {
					continue;
				}

				$address = CustomerAddress::model()->findByAttributes([
					'address'    => $addressTitle,
					'customerId' => $customerParams['id'],
				]);

				if ($address === null) {
					$address = new CustomerAddress();
					$address->address = $addressTitle;
					$address->customerId = $customerParams['id'];
				}

				if ($this instanceof ConcreteOrder) {
					$field = 'concreteOrderId';
				} elseif ($this instanceof SpecOrder) {
					$field = 'specOrderId';
				} else {
					throw new CException('Unknown $this instance');
				}

				$concreteOrderIds = explode(',', $address->$field);
				$concreteOrderIds = array_filter($concreteOrderIds);
				$concreteOrderIds[] = $this->id;
				$concreteOrderIds = array_unique($concreteOrderIds);
				$address->$field = implode(',', $concreteOrderIds);

				$address->save();
			}
		}
	}

	/**
	 * Чтобы избежать повторения атрибутов для [3]item или item[3]
	 *
	 * @author Lukin A.
	 *
	 * @param $values
	 * @return array
	 */
	protected static function processLabels($values) {
		$additionalValues = [];

		foreach ($values as $key => $value) {
			for ($i = -1; $i <= 500; $i++) {
				$prefix = '';
				if ($i > -1) {
					$prefix = '[' . $i . ']';
				}
				$additionalValues[$prefix . $key] = $value;
				$additionalValues[$key . $prefix] = $value;
			}
		}

		return $additionalValues;
	}

	public static function _class_() {
		return __CLASS__;
	}

	public static function stripDate($date) {
		return str_replace('T', ' ', $date);
	}

	public function all() {
		$this->resetScope();
		return $this;
	}

	public function scopes() {
		return [
			'active' => [
				'condition' => 't.status != :active',
				'params'    => [':active' => self::STATUS_CANCELLED],
			]
		];
	}

	public function setAttributes($values, $safeOnly = true) {
		if (!is_array($values)) {
			return;
		}
		$attributes = array_flip($safeOnly? $this->getSafeAttributeNames(): $this->attributeNames());
		foreach ($values as $name => $value) {
			$nameRest = substr($name, 1);
			$func = 'set' . strtoupper($name[0]) . $nameRest;
			if (method_exists($this, $func)) {
				$this->$func($value);
			} else {
				if (isset($attributes[$name])) {
					$this->$name = $value;
				} else {
					if ($safeOnly) {
						$this->onUnsafeAttribute($name, $value);
					}
				}
			}
		}
	}

	/** @return bool */
	public function isExists() {
		$pk = array();
		$table = $this->getMetaData()->tableSchema;
		if (is_string($table->primaryKey)) {
			$pk[$table->primaryKey] = $this->{$table->primaryKey};
		} elseif (is_array($table->primaryKey)) {
			foreach ($table->primaryKey as $name) {
				$pk[$name] = $this->$name;
			}
		} else {
			return false;
		}
		return $this->countByAttributes($pk);
	}

	/**
	 * Scope for 1 month back
	 *
	 * @author Lukin A.
	 *
	 * @return $this
	 */
	public function thisMonth() {
		$criteria = $this->getDbCriteria();
		$criteria->compare('date', new CDbExpression('>= DATE_SUB(NOW() - INTERVAL 1 MONTH)'));
		return $this;
	}

	/**
	 * Scope for this day and {self::EXPORT_ORDERS_DAYS_FORWARD} days forward
	 *
	 * @author Lukin A.
	 *
	 * @param int $daysCount
	 * @return $this
	 */
	public function todayWithDaysForward($daysCount = self::EXPORT_ORDERS_DAYS_FORWARD) {
		$criteria = $this->getDbCriteria();
		$criteria->addCondition('t.date BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY) AND DATE_ADD(CURRENT_DATE(), INTERVAL ' . ($daysCount + 1) . ' DAY)');
		return $this;
	}

	/**
	 * Scope for nearest date
	 *
	 * @author Lukin A.
	 *
	 * @param string $date
	 * @return $this
	 */
	public function nearest($date) {
		$date = self::stripDate($date); // because it comes with 01.02.2012T05:00:00 format
		$criteria = $this->getDbCriteria();
//		$criteria->addBetweenCondition('t.date', "DATE_SUB('" . $date . "', INTERVAL " . self::NEAREST_ORDERS_HOURS . ' HOUR)', "DATE_ADD('" . $date . "', INTERVAL " . self::NEAREST_ORDERS_HOURS . ' HOUR)');
		$criteria->addCondition("t.date BETWEEN DATE_SUB('" . $date . "', INTERVAL " . self::NEAREST_ORDERS_HOURS . " HOUR) AND DATE_ADD('" . $date . "', INTERVAL " . self::NEAREST_ORDERS_HOURS . ' HOUR)');
		return $this;
	}

	/**
	 * Scope for orders for all day in date
	 *
	 * @author Lukin A.
	 *
	 * @param string $date
	 * @return $this
	 */
	public function thisDay($date) {
		$date = date('Y.m.d', strtotime(self::stripDate($date)));
		$criteria = $this->getDbCriteria();
		$criteria->addCondition("DATE(t.date) = '{$date}'");
		return $this;
	}

	/**
	 * Return the start date well formatted
	 *
	 * @author Lukin A.
	 *
	 * @return bool|string
	 */
	public function getDateStart() {
		return date('Y-m-d\TH:i:00', strtotime($this->date));
	}

	/**
	 * Return the end date well formatted
	 *
	 * @author Lukin A.
	 *
	 * @return bool|string
	 */
	public function getDateEnd() {
		return date('Y-m-d\TH:i:00', strtotime($this->date) + 1800);
	}

	public function getDateWoTime() {
		return date('Y-m-d', strtotime($this->date));
	}

	public function getDateTime() {
		return date('H:i', strtotime($this->date));
	}

	/**
	 * Устанавливает предел для запроса
	 *
	 * @author Lukin A.
	 *
	 * @param int $limit
	 * @return $this
	 */
	public function limit($limit = 50) {
		$criteria = new CDbCriteria();
		$criteria->limit = $limit;

		$this->getDbCriteria()->mergeWith($criteria);
		return $this;
	}

	/**
	 * Сортировка по указанному атрибуту
	 *
	 * Может быть указан атрибут из связанной модели.
	 *
	 * @param string $attributeName Название атрибута
	 * @param string $direction     По возрастанию или убыванию
	 * @return $this
	 */
	public function sort($attributeName, $direction = 'asc') {
		$direction = strtoupper($direction);

		// -- Если атрибут относится к текущей модели, добавляем алиас таблицы (на всякий случай, чтобы избежать возможного конфликта имён)
		if (strpos($attributeName, '.') === false) {
			$attributeName = sprintf('`%s`.`%s`', $this->getTableAlias(), $attributeName);
		}
		// -- -- -- --

		// -- Подключаем критерию
		$this->getDbCriteria()->mergeWith([
			'order' => $attributeName . ' ' . $direction,
		]);
		// -- -- -- --

		return $this;
	}
}
