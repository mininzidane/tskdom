<?php

class m141202_154639_drop_foreign_keys extends DbMigration {

	public function safeUp() {
		$this->dropForeignKey('concrete_order_customer', 'concrete_order');
	}

	public function safeDown() {
		$this->addForeignKey('concrete_order_customer', 'concrete_order', 'customerId', 'customer', 'id');
	}
}
