<?php

class m141201_015207_init_spec_model_tables_add_status_field_for_order extends DbMigration {

	public function safeUp() {
		$this->createTable('spec_type', [
			'id'      => 'INT(3) UNSIGNED NOT NULL AUTO_INCREMENT',
			'title'   => 'VARCHAR(128) NOT NULL',
			'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);
		$this->createTable('spec_model', [
			'id'         => 'INT(4) UNSIGNED NOT NULL AUTO_INCREMENT',
			'title'      => 'VARCHAR(128) NOT NULL',
			'specTypeId' => 'INT(3) UNSIGNED NOT NULL',
			'created'    => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);
		$this->addForeignKey('spec_model_to_spec_type', 'spec_model', 'specTypeId', 'spec_type', 'id');

		$this->addColumn('concrete_order', 'status', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER customerId');
		$this->addColumn('spec_order', 'status', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER customerId');
	}

	public function safeDown() {
		$this->dropColumn('concrete_order', 'status');
		$this->dropColumn('spec_order', 'status');
		$this->dropForeignKey('spec_model_to_spec_type', 'spec_model');
		$this->dropTable('spec_model');
		$this->dropTable('spec_type');
	}
}
