<?php

class m150201_151602_add_highlight_column_to_a_cement_table extends DbMigration {

	public function safeUp() {
		$this->addColumn('cement', 'highlight', 'TINYINT(1) NOT NULL DEFAULT 0 AFTER isUpdated');
	}

	public function safeDown() {
		$this->dropColumn('cement', 'highlight');
	}
}
