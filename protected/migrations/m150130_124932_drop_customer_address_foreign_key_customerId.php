<?php

class m150130_124932_drop_customer_address_foreign_key_customerId extends DbMigration {

	public function safeUp() {
		$this->dropForeignKey('customer_address_customer', 'customer_address');
	}

	public function safeDown() {
		$this->addForeignKey('customer_address_customer', 'customer_address', 'customerId', 'customer', 'id');
	}
}
