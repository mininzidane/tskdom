<?php

class TipperController extends BaseController {

	public function filters() {
		return ['accessControl'];
	}

	// admin actions
	public function accessRules() {
		return [
			['allow', 'roles' => [WebUser::ROLE_MANAGER]],
			['deny', 'users' => ['*']],
		];
	}

	public function actionIndex() {
		$filter = Yii::app()->request->getParam('filter');
		if ($filter !== null) {
			$filter = array_filter($filter);
			$models = Tipper::model()->findAllByAttributes($filter);
		} else {
			$models = Tipper::model()->findAll();
		}

		$this->render('index', [
			'filter' => $filter,
			'models' => $models,
		]);
	}

	public function actionAdd() {
		$model = new Tipper();

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost(get_class($model));
			if ($model->save()) {
				$this->redirect(['index']);
			}
		}

		$this->render('edit', [
			'model' => $model,
		]);
	}

	public function actionEdit($id) {
		$model = Tipper::model()->findByPk($id);

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost(get_class($model));
			if ($model->save()) {
				$this->redirect(['index']);
			}
		}

		$this->render('edit', [
			'model' => $model,
		]);
	}
}