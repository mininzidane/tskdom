<?php

class m141210_130928_init_logistic_table extends DbMigration {

	public function safeUp() {
		$this->createTable('logistic', [
			'id'           => 'INT(5) UNSIGNED NOT NULL AUTO_INCREMENT',
			'shippingDate' => 'DATE NULL DEFAULT NULL',
			'shippingFrom' => 'VARCHAR(255) NULL DEFAULT NULL',
			'cargo'        => 'VARCHAR(255) NULL DEFAULT NULL',
			'attachment'   => 'VARCHAR(255) NULL DEFAULT NULL',
			'transport'    => 'VARCHAR(255) NULL DEFAULT NULL',
			'arrivalDate'  => 'DATE NULL DEFAULT NULL',
			'arrivalCity'  => 'VARCHAR(255) NULL DEFAULT NULL',
			'arrivalPlace' => 'VARCHAR(255) NULL DEFAULT NULL',
			'status'       => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
			'created'      => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);
	}

	public function safeDown() {
		$this->dropTable('logistic');
	}
}
