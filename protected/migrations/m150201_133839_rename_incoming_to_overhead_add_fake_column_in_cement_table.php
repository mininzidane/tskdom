<?php

class m150201_133839_rename_incoming_to_overhead_add_fake_column_in_cement_table extends DbMigration {

	public function safeUp() {
		$this->renameColumn('cement', 'incoming', 'overhead');
		$this->addColumn('cement', 'fake', 'FLOAT NULL DEFAULT NULL AFTER overhead');
	}

	public function safeDown() {
		$this->dropColumn('cement', 'fake');
		$this->renameColumn('cement', 'overhead', 'incoming');
	}
}
