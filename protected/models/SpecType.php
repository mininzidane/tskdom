<?php

/**
 * Модель типа спецтехники
 *
 * @author Lukin A.
 *
 * @property int    $id
 * @property string $title
 */
class SpecType extends ActiveRecord {

	/**
	 * @param string $className
	 * @return SpecType
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'spec_type';
	}

	public function relations() {
		return [
			'specModels' => [self::HAS_MANY, 'SpecModel', ['specTypeId' => 'id']],
		];
	}

	public function rules() {
		return [
			['title', 'required'],
		];
	}

	public function attributeLabels() {
		return [
			'title'   => 'Название типа',
			'title[]' => 'Название типа',
		];
	}
}
