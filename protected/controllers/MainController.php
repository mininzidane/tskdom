<?php

class MainController extends BaseController {

	public function filters() {
		return ['accessControl'];
	}

	// admin actions
	public function accessRules() {
		return [
			['allow', 'actions' => ['signIn', 'signOut', 'calculator'], 'users' => ['*']],

			['allow', 'actions' => ['editSpecModels, editSpecTypes'], 'roles' => [WebUser::ROLE_ADMIN]],
			['deny', 'actions' => ['editSpecModels, editSpecTypes'], 'users' => ['*']],

			['allow', 'actions' => ['specOrder', 'index'], 'roles' => [WebUser::ROLE_MANAGER]],
			['deny', 'actions' => ['specOrder', 'index'], 'roles' => ['*']],

			['allow', 'users' => ['@']],
			['deny', 'users' => ['*']],
		];
	}

	public function actionSignIn() {
		if (!Yii::app()->user->isGuest) {
			$this->redirect($this->createUrl('main/journal'));
		}

		$model = new SignInForm();

		// collect user input data
		if (isset($_POST['SignInForm'])) {
			$model->attributes = $_POST['SignInForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login()) {
				if (Yii::app()->user->returnUrl === '/') {
					Yii::app()->user->returnUrl = ['main/journal'];
				}
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
		// display the login form
		$this->render('sign-in', array('model' => $model));
	}

	public function actionSignOut() {
		/** @var $user CWebUser */
		$user = Yii::app()->user;
		$user->logout();
		Yii::app()->request->redirect($this->createUrl('journal'));
	}

	public function actionIndex() {
		$model = new ConcreteOrder();
		$customer = new Customer();
		$address = new CustomerAddress();

		// save form data
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost(get_class($model));

			$transaction = Yii::app()->db->beginTransaction();

			try {
				$success = true;

				$customer = Customer::saveCustomer();

				if ($customer) {
					$model->customerId = $customer->id;

					if ($model->save() && $success) {
						$transaction->commit();
						Yii::app()->user->setFlash($this->action->id, true);
						$this->redirect(['index']);
					} else {
						Yii::app()->user->setFlash($this->action->id, false);
					}
				}
			} catch (Exception $e) {
				$transaction->rollback();
			}
		}

		/** @var ConcreteOrder[] $concreteOrders */
		$concreteOrders = ConcreteOrder::model()
			->with('customer.addresses')
			->active()
			->thisMonth()
			->findAll();

		$orders = [];
		foreach ($concreteOrders as $concreteOrder) {
			$orders[] = [
				'title' => "{$concreteOrder->volume} м3",
				'start' => $concreteOrder->getDateStart(),
				'end'   => $concreteOrder->getDateEnd(),
			];
		}
		$orders = json_encode($orders);

		$this->render('concreteOrder', [
			'model'        => $model,
			'customer'     => $customer,
			'address'      => $address,
			'orders'       => $orders,
			'url'          => $this->createUrl('ajax/getNearestConcreteOrders'),
			'autoComplete' => Customer::getItemsForAutoComplete(),
		]);
	}

	public function actionSpecOrder() {
		$model = new SpecOrder();
		$customer = new Customer();
		$address = new CustomerAddress();

		// save form data
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost(get_class($model));

			$transaction = Yii::app()->db->beginTransaction();

			try {
				$success = true;

				$customer = Customer::saveCustomer();
				if ($customer) {
					$model->customerId = $customer->id;

					if ($model->save() && $success) {
						$transaction->commit();
						Yii::app()->user->setFlash($this->action->id, true);
						$this->redirect(['main/specOrder']);
					} else {
						Yii::app()->user->setFlash($this->action->id, false);
					}
				}
			} catch (Exception $e) {
				$transaction->rollback();
			}
		}

		/** @var SpecOrder[] $specOrders */
		$specOrders = SpecOrder::model()
			->with('customer.addresses')
			->thisMonth()
			->active()
			->findAll();

		$orders = [];
		foreach ($specOrders as $specOrder) {
			$orders[] = [
				'title' => "{$specOrder->spec->title}",
				'start' => $specOrder->getDateStart(),
				'end'   => $specOrder->getDateEnd(),
			];
		}
		$orders = json_encode($orders);

		// Спецтехника
		$specTypes = SpecType::model()->with('specModels')->findAll();

		$this->render('specOrder', [
			'model'        => $model,
			'customer'     => $customer,
			'address'      => $address,
			'orders'       => $orders,
			'specTypes'    => $specTypes,
			'url'          => $this->createUrl('ajax/getNearestSpecOrders'),
			'autoComplete' => Customer::getItemsForAutoComplete(),
		]);
	}

	public function actionJournal() {
		$this->render('journal', [
			'url'       => $this->createUrl('ajax/getNearestJournal'),
			'customers' => Customer::model()->findAll(),
			'allSpecs'  => SpecModel::model()->findAll(),
		]);
	}

	public function actionEditSpecTypes() {
		/** @var SpecType[] $specTypes */
		$specTypes = SpecType::model()->findAll();
		$newSpecType = new SpecType();

		if (Yii::app()->request->isPostRequest) {
			$post = Yii::app()->request->getPost(get_class($newSpecType));
			$newModels = [];

			foreach ($post as $specTypeId => $values) {
				if (is_numeric($specTypeId)) { // если существующая модель
					$specType = SpecType::model()->findByPk($specTypeId);
					$specType->attributes = $values;
					$specType->save();
				} else { // если новая модель
					// в этом случае у нас для каждого атрибута дается массив значений
					foreach ($post[$specTypeId] as $i => $value) {
						if (!array_key_exists($i, $newModels)) {
							$newModels[$i] = new SpecType();
						}

						/** @var SpecType $specType */
						$specType = $newModels[$i];
						$specType->{$specTypeId} = $value;
					}
				}
			}

			// сохраняем все модели из массива
			foreach ($newModels as $newModel) {
				/** @var SpecType $newModel */
				$newModel->save();
			}

			$this->redirect(['editSpecTypes']);
		}

		$this->render('editSpecTypes', [
			'specTypes'   => $specTypes,
			'newSpecType' => $newSpecType,
		]);
	}

	public function actionEditSpecModels() {
		/** @var SpecType[] $specModels */
		$specModels = SpecModel::model()->with('specType')->findAll();
		$newSpecModel = new SpecModel();

		if (Yii::app()->request->isPostRequest) {
			$post = Yii::app()->request->getPost(get_class($newSpecModel));
			$newModels = [];

			foreach ($post as $specModelId => $values) {
				if (is_numeric($specModelId)) { // если существующая модель
					$specModel = SpecModel::model()->findByPk($specModelId);
					$specModel->attributes = $values;
					$specModel->save();
				} else { // если новая модель
					// в этом случае у нас для каждого атрибута дается массив значений
					foreach ($post[$specModelId] as $i => $value) {
						if (!array_key_exists($i, $newModels)) {
							$newModels[$i] = new SpecModel();
						}

						/** @var SpecModel $specModel */
						$specModel = $newModels[$i];
						$specModel->{$specModelId} = $value;
					}
				}
			}

			// сохраняем все модели из массива
			foreach ($newModels as $newModel) {
				/** @var SpecModel $newModel */
				$newModel->save();
			}

			$this->redirect(['editSpecModels']);
		}

		$this->render('editSpecModels', [
			'specModels'   => $specModels,
			'data'         => CHtml::listData(SpecType::model()->findAll(), 'id', 'title'),
			'newSpecModel' => $newSpecModel,
		]);
	}

	public function actionLogistic($page = 1) {
		$model = new Logistic();

		if (Yii::app()->request->isPostRequest) {
			$success = true;
			$model->attributes = Yii::app()->request->getPost(get_class($model));

			if (Yii::app()->request->getPost('attachment') || $_FILES[get_class($model)]['name']['attachment'] !== '') {
				$field = 'attachment';
				$files = $_FILES[get_class($model)];
				$fileSource = $files['tmp_name'][$field];
				$extension = pathinfo($files['name'][$field], PATHINFO_EXTENSION);
				$newFileName = Logistic::getAttachmentsPath() . md5_file($fileSource) . '.' . $extension;

				$path = Yii::getPathOfAlias('webroot') . Logistic::getAttachmentsPath();
				if (!file_exists($path)) {
					mkdir($path, 0777, true);
				}

				if (file_put_contents(Yii::getPathOfAlias('webroot') . $newFileName, file_get_contents($fileSource)) !== false) {
					$model->attachment = $newFileName;
				} else {
					$success = false;
				}
			}

			if ($success && $model->save()) {
				Yii::app()->user->setFlash($this->action->id, true);
				$this->redirect(['logistic']);
			} else {
				Yii::app()->user->setFlash($this->action->id, false);
			}
		}

		$criteria = Logistic::model()
			->getDbCriteria();

		$pages = new CPagination(Logistic::model()->count());
		$pages->pageSize = 100;
		$pages->currentPage = $page - 1; // потому что пагинатор считает с 0
		$pages->applyLimit($criteria);

		/** @var Logistic[] $logistics */
		$logistics = Logistic::model()->findAll($criteria);

		$this->render('logistic', [
			'logistics' => $logistics,
			'model'     => $model,
		]);
	}

	public function actionEditLogistic($id) {
		$logistic = Logistic::model()->findByPk($id);

		if ($logistic === null) {
			throw new CHttpException(404, 'Запись логистики не найдена');
		}

		$post = Yii::app()->request->getPost(get_class($logistic));
		if ($post !== null) {
			$logistic->attributes = $post;

			if ($logistic->save()) {
				Yii::app()->user->setFlash('logistic', true);
				$this->redirect(['logistic']);
			} else {
				Yii::app()->user->setFlash($this->action->id, false);
			}
		}

		$this->render('editLogistic', [
			'model' => $logistic
		]);
	}

	public function actionCalculator() {
		$this->render('calculator');
	}

	public function actionCement() {
		$cements = [];
		$remainsByCityAndSilages = [];

		/** @var Cement[] $models */
		$models = Cement::model()->thisMonth()->findAll();

		// -- Сначала считаем общее кол-во по силосам в городах
		foreach ($models as $cement) {
			$remainsByCityAndSilages[$cement->date][$cement->city]['dates'] = [
				$cement->getDateStart(),
				$cement->getDateEnd(),
			];
			$remainsByCityAndSilages[$cement->date][$cement->city]['silages'][$cement->silage] = $cement->remains;
		}
		// -- -- -- --

		// -- Затем заполняем в удобной форме для календаря
		foreach ($remainsByCityAndSilages as $cities) {
			foreach ($cities as $city => $data) {
				$cements[] = [
					'title' => $city  .': ' . array_sum($data['silages']) . ' т',
					'start' => $data['dates'][0],
					'end'   => $data['dates'][1],
				];
			}
		}
		// -- -- -- --

		$this->render('cement', [
			'cements' => json_encode($cements),
			'url'     => $this->createUrl('ajax/getCementStat'),
		]);
	}
}