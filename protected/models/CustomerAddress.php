<?php

/**
 * Class CustomerAddress
 *
 * @author Lukin A.
 *
 * @property int    $id
 * @property string $address
 * @property int    $customerId
 * @property string $concreteOrderId
 * @property string $specOrderId
 */
class CustomerAddress extends ActiveRecord {

	/**
	 * @param string $className
	 * @return CustomerAddress
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'customer_address';
	}

	public function rules() {
		return [
			['address', 'required'],
			['concreteOrderId, specOrderId', 'safe'],
		];
	}

	public function attributeLabels() {
		return [
			'address[]' => 'Адрес объекта',
		];
	}
}
