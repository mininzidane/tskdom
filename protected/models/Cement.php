<?php

/**
 * Class Cement
 *
 * @author Lukin A.
 *
 * @property int    $id
 * @property string $date
 * @property string $city
 * @property int    $silage
 * @property float  $outgo
 * @property float  $overhead
 * @property float  $fake
 * @property float  $difference
 * @property float  $remains
 * @property int    $driverId
 * @property bool   $isUpdated
 * @property string $updated
 *
 * @property Driver $driver
 */
class Cement extends ActiveRecord {

	/**
	 * @var bool Флаг, устанавливаемый для моделей цемента, рассчитываемых на следующий день. Нужен, чтобы,
	 * не имея галочки isEmpty в POST запросе, определять по остаткам (если они равны 0), что силос в этот день ПУСТ
	 */
	public $isCalculatingNextDay = false;

	const CITY_VLADIVOSTOK  = 'Владивосток';
	const CITY_KHABAROVSK_1 = 'Хабаровск-1';
	const CITY_KHABAROVSK_2 = 'Хабаровск-2';
	const CITY_MATVEEVKA    = 'Матвеевка';

	const CEMENT_FILENAME = 'cement-remains';

	/** @var array Максимальная вместимость силосов по городам. Числа идут по номерам силосов */
	public static $silages = [
		self::CITY_VLADIVOSTOK  => [
			1 => 110,
			2 => 110,
			3 => 110,
		],
		self::CITY_KHABAROVSK_1 => [
			1 => 110,
			2 => 110,
			3 => 110,
		],
		self::CITY_KHABAROVSK_2 => [
			1 => 110,
			2 => 110,
		],
		self::CITY_MATVEEVKA    => [
			1 => 60,
		],
	];

	/**
	 * @param string $className
	 * @return Cement
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return [
			['date, city, silage, outgo, overhead, driverId, isUpdated', 'required'],
			['city', 'in', 'range' => array_keys(self::$silages)],
			['silage', 'numerical', 'integerOnly' => true],
			['fake, highlight', 'safe'],
		];
	}

	public function relations() {
		return [
			'driver' => [self::BELONGS_TO, 'Driver', 'driverId'],
		];
	}


	public function attributeLabels() {
		 $values = [
			 'silage'     => '№ силоса',
			 'outgo'      => 'Расход, т',
			 'incoming'   => 'Приход, т',
			 'overhead'   => 'Накладные, т',
			 'fake'       => 'Левый, т',
			 'difference' => 'Экономия/недостача, т',
			 'highlight'  => 'Пометка',
		];

		return $values;
	}

	/**
	 * Расчитываем остатки перед записью в базу
	 *
	 * @author Lukin A.
	 *
	 * @return bool
	 */
	protected function beforeSave() {
		$this->calculateSilageValue();
		return parent::beforeSave();
	}

	/**
	 * Расчитываем дни после измененного
	 *
	 * @author Lukin A.
	 *
	 * @return bool
	 */
	protected function afterSave() {
		$this->_calculateNextDay();
		parent::afterSave();
	}

	/**
	 * Пересчитываем остатки по каждому следующему дню подряд, до прерывания сэта дней
	 *
	 * @author Lukin A.
	 *
	 * @throws CDbException
	 */
	private function _calculateNextDay() {
		$dateDayNext = date('Y-m-d', strtotime($this->date) + 24 * 3600);

		/** @var Cement $cementDayNext */
		$cementDayNext = self::model()->findByAttributes([
			'date'   => $dateDayNext,
			'city'   => $this->city,
			'silage' => $this->silage,
		]);

		if ($cementDayNext !== null) {
			$cementDayNext->isCalculatingNextDay = true;
			$cementDayNext->calculateSilageValue();
			$cementDayNext->saveAttributes(['remains', 'difference']);
			$cementDayNext->_calculateNextDay();
		}
	}

	/**
	 * Расчет остатков в силосе
	 *
	 * @author Lukin A.
	 *
	 * @param float $remains    Остаток за предыдущий день
	 * @return float
	 */
	public function calculateRemainsValue($remains) {
		$post = Yii::app()->request->getPost(__CLASS__);

		// -- Если кончился силос
		$isEmpty = false;
		if ($post !== null) {
			$firstElement = current($post);
			if (is_array($firstElement)) {
				$isEmpty = (bool) $firstElement['isEmpty'];
			} else {
				$isEmpty = (bool) $post['isEmpty'];
			}
		}

		if ($this->isCalculatingNextDay) {
			$isEmpty = $this->remains == 0;
		}
		// -- -- -- --

		// расход
		$outgo = ($this->fake === null? 0: $this->fake) + $this->overhead - $this->outgo;

		if ($outgo < 0 && ($remains < abs($outgo) || $remains > abs($outgo) && $isEmpty)) {
			$this->difference = abs($outgo) - $remains;
			return 0;
		} else {
			$this->difference = 0;
		}

		return ($remains + $outgo + $this->difference);
	}

	/**
	 * Расчет остатков на дату по городу и номеру силоса
	 *
	 * @author Lukin A.
	 */
	public function calculateSilageValue() {
		$dateDayBefore = date('Y-m-d', strtotime($this->date) - 24 * 3600);

		/** @var Cement $cementDayBefore */
		$cementDayBefore = self::model()->findByAttributes([
			'date'   => $dateDayBefore,
			'city'   => $this->city,
			'silage' => $this->silage,
		]);

		$this->remains = ($cementDayBefore === null? 0: $this->calculateRemainsValue($cementDayBefore->remains));
	}

	/**
	 * Возвращает процентное содержание остатков от общей вместимости силоса
	 *
	 * @author Lukin A.
	 *
	 * @return float
	 */
	public function getPercentageSilageValue() {
		if (!isset(self::$silages[$this->city][$this->silage])) {
			return 0;
		}

		$silageMaxValue = self::$silages[$this->city][$this->silage];

		$percentage = round($this->remains / $silageMaxValue, 2);
		return $percentage;
	}

	/**
	 * Получает имя файла с остатками цемента
	 *
	 * @author Lukin A.
	 *
	 * @return string
	 */
	private static function _getCementRemainsFilename() {
		return Yii::getPathOfAlias('application.data') . DIRECTORY_SEPARATOR . self::CEMENT_FILENAME;
	}

	/**
	 * Получает остатки цемента
	 *
	 * @author Lukin A.
	 *
	 * @return string
	 */
	public static function getCementRemains() {
		$filename = self::_getCementRemainsFilename();
		return file_get_contents($filename);
	}

	/**
	 * Изменение остатков цемента
	 *
	 * @author Lukin A.
	 *
	 * @param $remains
	 * @return bool
	 */
	public static function setCementRemains($remains) {
		$filename = self::_getCementRemainsFilename();
		return (bool) file_put_contents($filename, $remains);
	}
}
