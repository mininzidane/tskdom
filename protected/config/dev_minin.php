<?php

Yii::import('system.collections.CMap');

$config = CMap::mergeArray(require('_main.php'), [
		'components' => [
			'db'  => [
				'connectionString'   => 'mysql:host=localhost;dbname=tskdom',
				'emulatePrepare'     => true,
				'username'           => 'root',
				'password'           => '28051989',
				'charset'            => 'utf8',
				'enableParamLogging' => true,
			],
			'log' => [
				'class'  => 'CLogRouter',
				'routes' => [
					[
						'class'  => 'CFileLogRoute',
						'levels' => 'error, warning, info, trace',
					],
					[
						'class'  => 'CWebLogRoute',
						'levels' => 'error, warning, info, trace',
					],
				],
			],
		],
		'params'     => [
		],
	]
);

return $config;
