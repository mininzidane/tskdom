<?php

abstract class BaseController extends CController {

	public $layout = null;

	public function renderJson(array $data) {
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($data, defined('JSON_UNESCAPED_UNICODE')? JSON_UNESCAPED_UNICODE: 0);
		Yii::app()->end();
	}

	protected function beforeAction($action) {
		$controllerId = Yii::app()->controller->id;
		$actionId = Yii::app()->controller->action->id;
 
		Navigation::getMenu('main')
			->setWidget('application.widgets.Menu', ['activateItems' => false])
			->addItems([
				array(
					'id'      => 'specCatalog',
					'label'   => 'Каталог спецтехники',
					'url'     => (Yii::app()->user->role == WebUser::ROLE_KHAB_CEMENT_MANAGER? 'http://192.168.0.1/': 'http://192.168.1.40:800/'),
					'active'  => false,
					'weight'  => 20,
				),
				array(
					'id'      => 'journal',
					'label'   => 'Журнал заказов',
					'url'     => (Yii::app()->user->role == WebUser::ROLE_KHAB_CEMENT_MANAGER? 'http://192.168.0.1/journal': array('main/journal')),
					'active'  => $actionId == 'journal',
					'weight'  => 20,
					'visible' => true,
				),
				array(
					'id'      => 'concrete',
					'label'   => 'Заказ бетона',
					'url'     => (Yii::app()->user->role == WebUser::ROLE_KHAB_CEMENT_MANAGER? 'http://192.168.0.1/index': array('main/index')),
					'active'  => $controllerId == 'main' && $actionId == 'index',
					'weight'  => 10,
					'visible' => Yii::app()->user->checkAccess(WebUser::ROLE_MANAGER),
				),
				array(
					'id'      => 'spec',
					'label'   => 'Заказ спецтехники',
					'url'     => (Yii::app()->user->role == WebUser::ROLE_KHAB_CEMENT_MANAGER? 'http://192.168.0.1/specOrder': array('main/specOrder')),
					'active'  => $actionId == 'specOrder',
					'weight'  => 20,
					'visible' => Yii::app()->user->checkAccess(WebUser::ROLE_MANAGER),
				),
				array(
					'id'      => 'logistics',
					'label'   => 'Логистика',
					'url'     => (Yii::app()->user->role == WebUser::ROLE_KHAB_CEMENT_MANAGER? 'http://192.168.0.1/logistic': array('main/logistic')),
					'active'  => $actionId == 'logistic',
					'weight'  => 90,
				),
				array(
					'id'      => 'calculator',
					'label'   => 'Калькулятор',
					'url'     => (Yii::app()->user->role == WebUser::ROLE_KHAB_CEMENT_MANAGER? 'http://192.168.0.1/calculator': array('main/calculator')),
					'active'  => $actionId == 'calculator',
					'weight'  => 90,
				),
				array(
					'id'      => 'cement',
					'label'   => 'Журнал цемента',
					'url'     => (Yii::app()->user->role == WebUser::ROLE_KHAB_CEMENT_MANAGER? 'http://192.168.0.1/cement': array('main/cement')),
					'active'  => $actionId == 'cement',
					'weight'  => 90,
				),
				array(
					'id'      => 'editSpecModels',
					'label'   => 'Модели спецтехники',
					'url'     => array('main/editSpecModels'),
					'active'  => $actionId == 'editSpecModels',
					'weight'  => 90,
					'visible' => Yii::app()->user->checkAccess(WebUser::ROLE_ADMIN),
				),
				array(
					'id'      => 'editSpecTypes',
					'label'   => 'Типы спецтехники',
					'url'     => array('main/editSpecTypes'),
					'active'  => $actionId == 'editSpecTypes',
					'weight'  => 90,
					'visible' => Yii::app()->user->checkAccess(WebUser::ROLE_ADMIN),
				),
				array(
					'id'      => 'tippers',
					'label'   => 'Самосвалы',
					'url'     => array('tipper/index'),
					'active'  => $controllerId == 'tipper' && $actionId == 'index',
					'weight'  => 90,
					'visible' => Yii::app()->user->checkAccess(WebUser::ROLE_MANAGER),
				),
				array(
					'id'      => 'users',
					'label'   => 'Пользователи',
					'url'     => array('user/index'),
					'active'  => $controllerId == 'user',
					'weight'  => 90,
					'visible' => Yii::app()->user->checkAccess(WebUser::ROLE_ADMIN),
				),
			]);

		return parent::beforeAction($action); // TODO: Change the autogenerated stub
	}

	public function actionError() {
		$this->layout = 'error';
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				try {
					$this->render("error{$error['code']}", ['error' => $error]);
				} catch (CException $e) {
					$this->render("error", ['error' => $error]);
				}
			}
		}
	}
}
