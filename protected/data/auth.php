<?php

return [
	WebUser::ROLE_USER                => [
		'type'        => CAuthItem::TYPE_ROLE,
		'description' => 'Normal User',
		'children'    => [], // extend guest
		'bizRule'     => null,
		'data'        => null
	],
	WebUser::ROLE_MANAGER             => [
		'type'        => CAuthItem::TYPE_ROLE,
		'description' => 'Manager',
		'children'    => [WebUser::ROLE_USER], // extend user
		'bizRule'     => null,
		'data'        => null
	],
	WebUser::ROLE_CEMENT_MANAGER      => [
		'type'        => CAuthItem::TYPE_ROLE,
		'description' => 'Родительский класс для цементных менеджеров',
		'children'    => [], // extend guest
		'bizRule'     => null,
		'data'        => null
	],
	WebUser::ROLE_LOGISTICIAN         => [
		'type'        => CAuthItem::TYPE_ROLE,
		'description' => 'Логист Владивосток',
		'children'    => [WebUser::ROLE_CEMENT_MANAGER], // extend cement manager
		'bizRule'     => null,
		'data'        => null
	],
	WebUser::ROLE_KHAB_CEMENT_MANAGER => [
		'type'        => CAuthItem::TYPE_ROLE,
		'description' => 'Менеджер по цементу Хабаровск',
		'children'    => [WebUser::ROLE_CEMENT_MANAGER], // extend cement manager
		'bizRule'     => null,
		'data'        => null
	],
	WebUser::ROLE_ADMIN               => [
		'type'        => CAuthItem::TYPE_ROLE,
		'description' => 'Administrator',
		'children'    => [WebUser::ROLE_MANAGER, WebUser::ROLE_LOGISTICIAN, WebUser::ROLE_KHAB_CEMENT_MANAGER], // extend all
		'bizRule'     => null,
		'data'        => null
	],
];
