<?php

class m141127_153140_init_user_table extends DbMigration {

	public function safeUp() {
		$this->createTable('user', [
			'id'          => 'INT(4) UNSIGNED NOT NULL AUTO_INCREMENT',
			'username'    => 'varchar(128) NOT NULL',
			'password'    => 'varchar(128) NOT NULL',
			'salt'        => 'varchar(100) NOT NULL DEFAULT ""',
			'email'       => 'varchar(200) DEFAULT NULL',
			'realname'    => 'varchar(200) DEFAULT NULL',
			'description' => 'varchar(200) DEFAULT NULL',
			'role'        => 'varchar(128) DEFAULT NULL',
			'PRIMARY KEY (id)',
		]);
		$this->insert('user', array(
			'username' => 'admin',
			'password' => '339919579cce6fc45c7befbc3e2fbd5c',
			'salt'     => '5062ac350fc3a5.00578474',
			'email'    => 'uik@tskdom.ru',
			'role'     => 'admin'
		));
	}

	public function safeDown() {
		$this->dropTable('user');
	}
}
