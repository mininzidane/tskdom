<?php

/**
 * Class Customer
 *
 * @author Lukin A.
 *
 * @property int               $id
 * @property string            $title
 * @property string            $phone
 *
 * @property CustomerAddress[] $addresses
 */
class Customer extends ActiveRecord {

	/**
	 * @param string $className
	 * @return Customer
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'customer';
	}


	public function relations() {
		return [
			'addresses' => [self::HAS_MANY, 'CustomerAddress', 'customerId'],
		];
	}

	public function rules() {
		return [
			['title', 'required'],
			['phone, comment', 'safe'],
		];
	}

	public function attributeLabels() {
		return [
			'title'   => 'Название компании, частного лица',
			'phone'   => 'Телефон',
			'comment' => 'Комментарии',
		];
	}

	/**
	 * Get imploded addresses
	 *
	 * @author Lukin A.
	 *
	 * @param string $glue For implode
	 *
	 * @return string
	 */
	public function getAddresses($glue = '; ') {
		$addresses = [];
		foreach ($this->addresses as $address) {
			$addresses[] = $address->address;
		}

		return implode($glue, $addresses);
	}

	/**
	 * Get proper addresses for orders
	 *
	 * @author Lukin A.
	 *
	 * @param int    $orderId
	 * @param string $type
	 * @return CustomerAddress[]
	 * @throws CException
	 */
	public function getAddressesForOrder($orderId, $type = null) {
		if ($type === null) {
			$type = ConcreteOrder::ORDER_TYPE;
		}

		switch ($type) {
			case ConcreteOrder::ORDER_TYPE:
				$field = 'concreteOrderId';
				break;
			case SpecOrder::ORDER_TYPE:
				$field = 'specOrderId';
				break;
			default:
				throw new CException('Unknown type');
		}

		$addresses = [];
		foreach ($this->addresses as $address) {
			$orderIds = explode(',', $address->$field);

			if (in_array($orderId, $orderIds)) {
				$addresses[] = $address;
			}
		}
		return $addresses;
	}

	/**
	 * Get imploded proper addresses for orders
	 *
	 * @author Lukin A.
	 *
	 * @param        $orderId
	 * @param null   $type
	 * @param string $glue for implode
	 * @return string
	 * @throws CException
	 */
	public function getImplodedAddressesForOrder($orderId, $type = null, $glue = ', ') {
		$addresses = $this->getAddressesForOrder($orderId, $type);

		$addressTitles = [];
		foreach ($addresses as $address) {
			$addressTitles[] = $address->address;
		}
		return implode($glue, $addressTitles);
	}

	public static function getItemsForAutoComplete() {
		/** @var Customer[] $items */
		$items = self::model()->with('addresses')->findAll();

		$json = [];
		foreach ($items as $item) {
			$addresses = [];
			foreach ($item->addresses as $address) {
				$addresses[] = $address->address;
			}

			$json[] = [
				'value' => $item->title,
				'data'  => [
					'id'        => $item->id,
					'phone'     => $item->phone,
					'addresses' => $addresses,
				],
			];
		}

		return json_encode($json);
	}

	/**
	 * Сохраняет нового заказчика
	 *
	 * @author Lukin A.
	 *
	 * @return bool|Customer
	 */
	public static function saveCustomer() {
		$addressParams = Yii::app()->request->getPost('CustomerAddress');
		$customerParams = Yii::app()->request->getPost(__CLASS__);
		$success = true;

		$customer = null;
		if (isset($customerParams['id']) && $customerParams['id'] > 0) {
			$customer = Customer::model()->findByPk($customerParams['id']);
		}
		if ($customer === null) {
			$customer = new Customer(); // strange
		}

		$customer->attributes = $customerParams;

		if ($customer->save()) {
			// -- Saving addresses
			if (isset($addressParams['address']) && is_array($addressParams['address']) && $addressParams['address'][0]) {
				foreach ($addressParams['address'] as $addressTitle) {
					if (!$addressTitle) {
						continue;
					}

					$address = CustomerAddress::model()->findByAttributes([
						'address'    => $addressTitle,
						'customerId' => $customer->id,
					]);

					if ($address !== null) {
						continue;
					}

					$address = new CustomerAddress();
					$address->address = $addressTitle;
					$address->customerId = $customer->id;

					if (!$address->save()) {
						$success = false;
					}
				}
			}

			// If there is not addresses
			if (count($customer->addresses) === 0) {
				$customer->addError('title', 'Необходимо указать хотя бы один адрес');
				$success = false;
			}
			// -- -- -- --
		} else {
			return false;
		}

		return $success? $customer: false;
	}
}
