<?php

class m141130_063009_init_spec_order_tables extends DbMigration {

	public function safeUp() {
		$this->createTable('spec_order', [
			'id'         => 'INT(5) UNSIGNED NOT NULL AUTO_INCREMENT',
			'specModel'  => 'INT(5) UNSIGNED NULL DEFAULT NULL',
			'date'       => 'TIMESTAMP NULL DEFAULT NULL',
			'customerId' => 'INT(5) UNSIGNED NULL DEFAULT NULL',
			'created'    => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (id)',
		]);
	}

	public function safeDown() {
		$this->dropTable('spec_order');
	}
}
