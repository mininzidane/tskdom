<?php

/**
 * Class Logistic
 *
 * @author Lukin A.
 *
 * @property int        $id
 * @property string     $shippingDate
 * @property string     $shippingFrom
 * @property string     $cargo
 * @property string     $attachment
 * @property string     $transport
 * @property string     $arrivalDate
 * @property string     $arrivalCity
 * @property string     $arrivalPlace
 * @property int        $status
 */
class Logistic extends ActiveRecord {
	public $attachment;

	// Статусы элементов логистики
	const STATUS_IN_TRANSIT = 0;
	const STATUS_COMPLETED  = 1;
	const STATUS_NOT_COMING = 2;

	// Виды транспорта
	const TRANSPORT_RAILROAD = 'railroad';
	const TRANSPORT_AUTO     = 'auto';
	const TRANSPORT_SEA      = 'sea';

	/**
	 * @param string $className
	 * @return Logistic
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public static function getAttachmentsPath() {
		return DIRECTORY_SEPARATOR . 'attachments' . DIRECTORY_SEPARATOR;
	}

	public function defaultScope() {
		return [
			'order' => 't.created DESC',
		];
	}

	/**
	 * Получаем список доступных видов транспорта
	 *
	 * @author Lukin A.
	 *
	 * @return array
	 */
	public function getTransports() {
		return [
			self::TRANSPORT_RAILROAD => 'ЖД',
			self::TRANSPORT_AUTO     => 'Авто',
			self::TRANSPORT_SEA      => 'Море',
		];
	}

	/**
	 * Получаем список статусов
	 *
	 * @author Lukin A.
	 *
	 * @return array
	 */
	public function getStatusTexts() {
		return [
			self::STATUS_IN_TRANSIT => 'В пути',
			self::STATUS_COMPLETED  => 'Доставлен',
			self::STATUS_NOT_COMING => 'Не вышел',
		];
	}

	public function rules() {
		return [
			['shippingDate, shippingFrom, cargo, transport, arrivalDate, arrivalCity, arrivalPlace', 'required'],
			['status', 'in', 'range' => array_keys($this->getStatusTexts())],
			['transport', 'in', 'range' => array_keys($this->getTransports())],
		];
	}

	public function attributeLabels() {
		return [
			'shippingDate' => 'Дата отправки',
			'shippingFrom' => 'Откуда',
			'cargo'        => 'Груз',
			'transport'    => 'Транспорт',
			'arrivalDate'  => 'Дата прибытия',
			'arrivalCity'  => 'Город прибытия',
			'arrivalPlace' => 'Место прибытия',
			'status'       => 'Статус',
			'attachment'   => 'Прикрепленный файл',
		];
	}
}
