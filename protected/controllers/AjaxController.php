<?php

class AjaxController extends BaseController {

	private function _excelHeaders($filename) {
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private", false);
		header("Content-Type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=\"" . iconv('UTF-8', 'CP1251', $filename) . "\";");
		header("Content-Transfer-Encoding:­ binary");
	}

	public function filters() {
		return ['accessControl'];
	}

	// admin actions
	public function accessRules() {
		return [
			['allow', 'actions' => ['getOrdersExcel']],
//			['allow', 'roles' => [WebUser::ROLE_USER], 'actions' => ['getNearestJournal']],
//			['allow', 'roles' => [WebUser::ROLE_MANAGER, WebUser::ROLE_CEMENT_MANAGER]],
			['allow', 'users' => ['@']],
			['allow', 'actions' => ['fillCementNextDay'], 'users' => ['*']],
			['deny', 'users' => ['*']],
		];
	}

	/**
	 * Вернет таблицу с бетоном по заказам возле указанной даты
	 *
	 * @author Lukin A.
	 *
	 * @param string $date
	 */
	public function actionGetNearestConcreteOrders($date) {
		$orders = ConcreteOrder::model()
			->nearest($date)
			->findAll();

		$this->render('concreteOrdersTable', [
			'orders' => $orders,
		]);
	}

	/**
	 * Вернет таблицу с бетоном по заказам возле указанной даты
	 *
	 * @author Lukin A.
	 *
	 * @param string $date
	 */
	public function actionGetNearestSpecOrders($date) {
		$orders = SpecOrder::model()
			->with('spec.specType')
			->nearest($date)
			->findAll();

		$this->render('specOrdersTable', [
			'orders' => $orders,
		]);
	}

	/**
	 * Журнал заказов в виде таблицы на весь день указанной даты
	 *
	 * @author Lukin A.
	 *
	 * @param string $date
	 */
	public function actionGetNearestJournal($date) {
		$concreteOrders = ConcreteOrder::model()->with('customer', 'customer.addresses')->thisDay($date)->findAll();
		$specOrders = SpecOrder::model()->with('customer', 'customer.addresses')->thisDay($date)->findAll();

		$orders = [];
		/** @var ConcreteOrder|SpecOrder $order */
		foreach (array_merge($concreteOrders, $specOrders) as $order) {
			$item = [
				'id'         => $order->id,
				'date'       => $order->getDateWoTime(),
				'time'       => $order->getDateTime(),
				'customerId' => $order->customerId,
				'customer'   => $order->customer,
				'status'     => $order->status,
				'comment'    => $order->comment,
			];

			if ($order instanceof ConcreteOrder) {  // заявка на бетон
				foreach (ConcreteOrder::$recipeConsist as $recipeItem) {
					$item[$recipeItem] = $order->getRecipeConsist($recipeItem);
				}

				$totalVolume = $order->getMixersTotalVolume();
				$item['volume'] = ($totalVolume? "{$totalVolume} из ": '') . $order->volume;
				$item['orderType'] = ConcreteOrder::ORDER_TYPE;
			}

			if ($order instanceof SpecOrder) { // заявка на спецтехнику
				$item['specModel'] = "{$order->spec->specType->title} {$order->spec->title}";
				$item['orderType'] = SpecOrder::ORDER_TYPE;
			}

			$orders[strtotime($item['time'])][] = $item;

			// sort by customer id
			usort($orders[strtotime($item['time'])], function ($a, $b) {
				return $a['customerId'] - $b['customerId'];
			});
		}
		ksort($orders);

		$this->render('concreteAndSpecOrdersFormTable', [
			'orders' => $orders,
		]);
	}

	/**
	 * Устанавливает статус для определенной заявки
	 *
	 * @author Lukin A.
	 *
	 * @param int    $orderId
	 * @param string $orderType
	 * @param int    $status
	 * @throws CHttpException
	 */
	public function actionSetOrderStatus($orderId, $orderType = 'concrete', $status = 1) {
		switch ($orderType) {
			case 'concrete':
				$order = ConcreteOrder::model()->findByPk($orderId);
				break;
			case 'spec':
				$order = SpecOrder::model()->findByPk($orderId);
				break;
			default:
				throw new CHttpException(500, 'Не верный тип заказа');
		}

		if (!in_array($status, [$order::STATUS_IN_PROGRESS, $order::STATUS_CANCELLED, $order::STATUS_COMPLETED])) {
			throw new CHttpException(500, 'Не верный статус заказа');
		}

		$order->status = $status;
		echo (int) $order->save();
		Yii::app()->end();
	}

	/**
	 * Обработчик изменения заказа
	 *
	 * @author Lukin A.
	 *
	 * @param int    $orderId
	 * @param string $orderType
	 * @throws CDbException
	 * @throws CHttpException
	 */
	public function actionChangeOrder($orderId, $orderType = 'concrete') {
		/** @var ConcreteOrder|SpecOrder $order */
		switch ($orderType) {
			case 'concrete':
				$order = ConcreteOrder::model()->with('customer.addresses')->findByPk($orderId);
				break;
			case 'spec':
				$order = SpecOrder::model()->with('customer.addresses')->findByPk($orderId);
				break;
			default:
				throw new CHttpException(500, 'Неверный тип заказа');
		}

		if (Yii::app()->request->isPostRequest) {
			$isDelete = Yii::app()->request->getPost('delete') === null? false: true;
			$isRecover = Yii::app()->request->getPost('recover') === null? false: true;
			$order->attributes = Yii::app()->request->getPost(get_class($order));

			if ($isDelete) {
				$order->status = ActiveRecord::STATUS_CANCELLED;
				echo (int) $order->save();
				Yii::app()->end();
			}

			if ($isRecover) {
				$order->status = ActiveRecord::STATUS_IN_PROGRESS;
				echo (int) $order->save();
				Yii::app()->end();
			}

			// сохраняем время
			$date = new DateTime(Yii::app()->request->getPost('date', $order->date));
			$date->setTime(Yii::app()->request->getPost('hours'), Yii::app()->request->getPost('minutes'));
			$order->date = $date->format('Y-m-d H:i');

			// обрабатываем рецепт бетона
			if ($order instanceof ConcreteOrder) {
				$recipe = Yii::app()->request->getPost('recipe');
				@$order->prepareRecipe(
					$recipe[ConcreteOrder::TYPE],
					$recipe[ConcreteOrder::VALUE],
					$recipe[ConcreteOrder::ADDITIVE],
					$recipe[ConcreteOrder::STARTING],
					$recipe[ConcreteOrder::SHWING],
					$recipe[ConcreteOrder::BAG],
					$recipe[ConcreteOrder::EXTRA_W],
					$recipe[ConcreteOrder::EXTRA_F],
					$recipe[ConcreteOrder::EXTRA_P]
				);
			}

			echo (int) $order->save();
			Yii::app()->end();
		}

		// готовим список спецтехники для заявки на технику
		$data = null;
		if ($order instanceof SpecOrder) {
			$data = CHtml::listData(SpecModel::model()->findAll(), 'id', 'title');
		}

		$customerData = [];
		/** @var Customer $customer */
		foreach (Customer::model()->findAll() as $customer) {
			$customerData[$customer->id] = $customer->title;
		}

		$this->render('orderForm', [
			'model'        => $order,
			'customer'     => $order->customer,
			'date'         => date('Y-m-d', strtotime($order->date)),
			'hours'        => date('H', strtotime($order->date)),
			'minutes'      => date('i', strtotime($order->date)),
			'data'         => $data,
			'orderType'    => $orderType,
			'allCustomers' => $customerData,
		]);
	}

	/**
	 * Редактирование заказчика
	 *
	 * @author Lukin A.
	 *
	 * @param int $customerId
	 * @throws CDbException
	 * @throws CHttpException
	 */
	public function actionCustomerEdit($customerId) {
		if ($customerId == 0) {
			$customer = new Customer();
		} else {
			$customer = Customer::model()->with('addresses')->findByPk($customerId);
		}

		$address = new CustomerAddress();

		if ($customer === null) {
			throw new CHttpException(404, 'Заказчик не найден');
		}

		if (Yii::app()->request->isPostRequest) {
			$isDelete = Yii::app()->request->getPost('delete') === null? false: true;

			if ($isDelete) {
				CustomerAddress::model()->deleteAllByAttributes(['customerId' => $customer->id]);
				echo (int) $customer->delete();
				Yii::app()->end();
			}

			$customer->attributes = Yii::app()->request->getPost(get_class($customer));
			$addressParams = Yii::app()->request->getPost(get_class($address));

			$success = true;
			foreach ($addressParams as $addressId => $addressAttributes) {
				if (is_numeric($addressId)) { // если существующая модель
					$address = CustomerAddress::model()->findByPk($addressId);
					$address->attributes = $addressAttributes;
					if (!$address->save()) {
						$success = false;
					}
				} else {
					// -- Для новых кастомеров надо бы сохраниться, чтобы иметь customerId
					$saved = null;
					if ($customer->isNewRecord) {
						$saved = $customer->save();
					}
					// -- -- -- --

					foreach ($addressAttributes as $addressTitle) {
						if (!$addressTitle) {
							continue;
						}

						if ($saved !== false) {
							// если нет такого адреса, то записываем
							$address = new CustomerAddress();
							$address->address = $addressTitle;
							$address->customerId = $customer->id;
							if (!$address->save()) {
								$success = false;
								Yii::log('Не удалось сохранить адрес ' . $addressTitle . '; ошибки: ' . var_dump($address->errors), CLogger::LEVEL_ERROR);
							}
						}
					}

					// уже сохранили модель
					if ($saved !== null) {
						echo (int) ($customer->save() && $success);
						Yii::app()->end();
					}
				}
			}

			echo (int) ($customer->save() && $success);
			Yii::app()->end();
		}

		$this->render('customerEdit', [
			'customer' => $customer,
			'address'  => $address,
		]);
	}

	/**
	 * Это можно заменить на универсальный
	 *
	 * @author Lukin A.
	 *
	 * @param $customerAddressId
	 */
	public function actionDeleteAddress($customerAddressId) {
		echo (int) CustomerAddress::model()->deleteByPk($customerAddressId);
	}

	/**
	 * Универсальный экшен для удаления моделей по текстовому идентификатору
	 *
	 * @author Lukin A.
	 *
	 * @param int    $id
	 * @param string $model
	 * @throws CHttpException
	 */
	public function actionDeleteModel($id, $model) {
		$model = strtolower($model);
		switch ($model) {
			case 'specmodel':
				$model = SpecModel::model();
				break;
			case 'spectype':
				$model = SpecType::model();
				break;
			case 'user':
				$model = User::model();
				break;
			case 'tipper':
				$model = Tipper::model();
				break;
			default:
				throw new CHttpException(400, 'Неверная модель');
		}

		echo (int) $model->deleteByPk($id);
		Yii::app()->end();
	}

	/**
	 * Универсальный метод для изменения атрибутов моделей
	 *
	 * @author Lukin A.
	 *
	 * @param int      $id ID модели
	 * @param string   $model Тип модели
	 * @param bool|int $value Новое значение
	 * @param string   $attribute Атрибут модели, значение которого будем менять
	 * @throws CHttpException
	 */
	public function actionToggleModelAttribute($id, $model, $value, $attribute = 'status') {
		$modelName = strtolower($model);
		switch ($modelName) {
			case 'logistic':
				$model = Logistic::model();
				break;
			default:
				throw new CHttpException(400, 'Неверная модель');
		}

		$result = (bool) ($model->updateByPk($id, [$attribute => $value]) > 0);

		if (Yii::app()->request->isAjaxRequest) {
			echo (int) $result;
			Yii::app()->end();
		} else {
			if ($result) {
				Yii::app()->user->setFlash($modelName, true);
			} else {
				Yii::app()->user->setFlash($modelName, false);
			}

			$this->redirect(["main/{$modelName}"]);
		}
	}

	/**
	 * Возвращает аяксовую инфу по цементу
	 *
	 * @author Lukin A.
	 *
	 * @param $date
	 */
	public function actionGetCementStat($date) {
		$date = ActiveRecord::stripDate($date); // cause it comes with 01.02.2012T05:00:00 format

		/** @var Cement[] $cements */
		$cements = Cement::model()->with('driver')->findAllByAttributes(['date' => $date]);

		$availableCementsByCity = [];
		foreach ($cements as $cement) {
			$availableCementsByCity[$cement->city][$cement->silage] = $cement;
		}

		// -- Сделано так, чтобы сохранить порядок по городам
		$cementsByCity = [];
		$model = new Cement();
		foreach (Cement::$silages as $city => $silagesByCity) {
			foreach ($silagesByCity as $key => $silageMaxCount) {
				if (isset($availableCementsByCity[$city][$key])) {
					$cementsByCity[$city][$key] = $availableCementsByCity[$city][$key];
				} else {
					$cementsByCity[$city][$key] = $model;
				}
			}

			ksort($cementsByCity[$city]);
		}
		// -- -- -- --

		if (Yii::app()->request->isPostRequest) {
			$post = Yii::app()->request->getPost(get_class($model));

			foreach ($post as $id => $row) {
				if (is_numeric($id)) {
					$model = Cement::model()->findByPk($id);
					$model->attributes = $row;
					$model->isUpdated = 1;
					break;
				}
			}

			$model->attributes = $post;
			$success = $model->save();

			Yii::app()->user->setFlash($this->action->id, $success);
			echo (int) $success;
			Yii::app()->end();
		}

		$drivers = CHtml::listData(Driver::model()->findAll(), 'id', 'name');

		$this->render('cementStats', [
			'cements'       => $cementsByCity,
			'drivers'       => $drivers,
			'cities'        => array_keys(Cement::$silages),
			'cementRemains' => Cement::getCementRemains(),
		]);
	}

	/**
	 * Экшен для экспорта цемента в эксель
	 *
	 * @author Lukin A.
	 *
	 * @param $city
	 */
	public function actionGetCementExcel($city) {
		// -- Вытаскиваем данные по заказам
		$sql = 'SELECT silage, `date`, driver.name AS driver, outgo, overhead, fake, difference, remains, isUpdated, highlight, updated
				FROM cement
				LEFT JOIN driver
					ON cement.driverId = driver.id
				WHERE city = :city
				ORDER BY silage, `date`';
		$cements = Yii::app()
			->db
			->createCommand($sql)
			->queryAll(true, [':city' => $city]);
		// -- -- -- --

		// -- Разбиваем по силосам (подходит, если таблицы идут друг под другом)
		$cementsBySilage = [];
		foreach ($cements as $cement) {
			$cementsBySilage[$cement['silage']][] = $cement;
		}
		// -- -- -- --

		// -- Разбиваем по датам
		$silages = [];
		$cementsByDate = [];
		foreach ($cements as $cement) {
			$cementsByDate[$cement['date']][$cement['silage']] = $cement;
			$silages[$cement['silage']] = 1;
		}
		$silages = array_keys($silages);
		// -- -- -- --

		// -- Заполним отсутствующие силосы пустыми массивами - для экселя
		foreach ($cementsByDate as &$cementByDate) {
			foreach ($silages as $silage) {
				if (!array_key_exists($silage, $cementByDate)) {
					$cementByDate[$silage] = [];
				}
			}
			ksort($cementByDate);
		}
		unset($cementByDate);
		// -- -- -- --

		$citySilages = [
			$city => [
				'cementsByDate' => $cementsByDate,
				'silages'       => $silages,
			]
		];

		$this->_excelHeaders('cement.xls');

		$this->render('/excel/excelCement', [
			'citySilages' => $citySilages,
		]);
	}

	public function actionGetCementExcelByPeriod($driverId = 0, $dateFrom = null, $dateTo = null, $city = '') {
		$where = ['city = :city'];
		if ($driverId > 0) {
			$where[] = "driver.id = {$driverId}";
		}
		if ($dateFrom) {
			$where[] = "cement.date >= date('{$dateFrom}')";
		}
		if ($dateTo) {
			$where[] = "cement.date <= date('{$dateTo}')";
		}
		$where = implode(' AND ', $where);

		// -- Вытаскиваем данные по заказам
		$sql = 'SELECT silage, `date`, driver.name AS driver, outgo, overhead, fake, difference, remains, isUpdated, highlight, updated
					FROM cement
					LEFT JOIN driver
						ON cement.driverId = driver.id
					WHERE ' . $where . '
					ORDER BY silage, `date`';

		$citySilages = [];
		$cities = $city? [$city]: array_keys(Cement::$silages);
		foreach ($cities as $city) {
			$cements = Yii::app()
				->db
				->createCommand($sql)
				->queryAll(true, [':city' => $city]);

			// -- Разбиваем по датам
			$silages = [];
			$cementsByDate = [];
			foreach ($cements as $cement) {
				$cementsByDate[$cement['date']][$cement['silage']] = $cement;
				$silages[$cement['silage']] = 1;
			}
			$silages = array_keys($silages);
			// -- -- -- --

			// -- Заполним отсутствующие силосы пустыми массивами - для экселя
			foreach ($cementsByDate as &$cementByDate) {
				foreach ($silages as $silage) {
					if (!array_key_exists($silage, $cementByDate)) {
						$cementByDate[$silage] = [];
					}
				}
				ksort($cementByDate);
			}
			unset($cementByDate);
			// -- -- -- --

			$citySilages[$city] = [
				'cementsByDate' => $cementsByDate,
				'silages'       => $silages,
			];
		}

		// -- -- -- --

		$this->_excelHeaders("cement_from_{$dateFrom}_to_{$dateTo}.xls");

		$this->render('/excel/excelCementByPeriod', [
			'citySilages' => $citySilages,
		]);
	}

	public function actionGetJournalExcelByPeriod($specId = 0, $dateFrom = null, $dateTo = null) {
		$conditions = [];
		if ($specId > 0) {
			$conditions[] = "specModel = {$specId}";
		}
		if ($dateFrom) {
			$conditions[] = "date >= date('{$dateFrom}')";
		}
		if ($dateTo) {
			$conditions[] = "date <= date('{$dateTo}')";
		}

		$specOrders = SpecOrder::model()->active()->findAll(implode(' AND ', $conditions));

		$this->_excelHeaders("spec_orders_from_{$dateFrom}_to_{$dateTo}.xls");

		$this->render('/excel/excelSpecOrders', [
			'specOrders' => $specOrders,
		]);
	}
	
	public function actionGetTipperExcel() {
		$filter = Yii::app()->request->getParam('filter', []);
		if (!$filter) {
			$filter = [];
		}

		$models = Tipper::model()->findAllByAttributes($filter);
		$this->_excelHeaders("tippers.xls");
		$this->render('/excel/excelTippers', [
			'models' => $models,
		]);
	}

	/**
	 * Изменяет остатки цемента
	 *
	 * @author Lukin A.
	 *
	 * @param int $remains Остатки цемента
	 */
	public function actionChangeCementRemains($remains) {
		echo (int) Cement::setCementRemains($remains);
		Yii::app()->end();
	}

	/**
	 * Форма учета миксеров для заказа
	 *
	 * @author Lukin A.
	 *
	 * @param int $orderId ID заказа бетона
	 */
	public function actionEditMixers($orderId) {
		$model = ConcreteMixer::model()->findByAttributes(['orderId' => $orderId]);

		if ($model === null) {
			$model = new ConcreteMixer();
			$model->orderId = $orderId;
		}

		if (Yii::app()->request->isPostRequest) {
			$post = Yii::app()->request->getPost(get_class($model));

			$mixerInfo = [];
			foreach ($post['number'] as $i => $number) {
				if (!array_key_exists($i, $post['quantity']) || !$number || !$post['quantity'][$i]) {
					continue;
				}

				$mixerInfo[] = [
					$number => $post['quantity'][$i]
				];
			}

			$model->mixerInfo = $mixerInfo;
			$success = $model->save();

			Yii::app()->user->setFlash($this->action->id, $success);
			echo (int) $success;
			Yii::app()->end();
		}

		$this->render('editMixers', [
			'model' => $model,
		]);
	}

	/**
	 * Экшен вывода экселя по заявкам. Резервное копирование
	 *
	 * @author Lukin A.
	 */
	public function actionGetOrdersExcel() {
		/** @var ConcreteOrder[] $concreteOrders */
		$concreteOrders = ConcreteOrder::model()
			->with('customer.addresses')
			->sort('date')
			->todayWithDaysForward()
			->findAll();

		/** @var SpecOrder[] $specOrders */
		$specOrders = SpecOrder::model()
			->with('customer.addresses')
			->sort('date')
			->todayWithDaysForward()
			->findAll();

		$this->_excelHeaders('orders_3days.xls');

		$this->render('/excel/excelOrders', [
			'concreteOrders' => $concreteOrders,
			'specOrders'     => $specOrders,
		]);
	}

	/**
	 * Отдает JSON с журнала заказов для текущего месяца
	 *
	 * @author lukin.a
	 */
	public function actionGetMonthOrders() {
		/** @var ConcreteOrder[] $concreteOrders */
		$concreteOrders = ConcreteOrder::model()
			->active()
			->with('customer.addresses')
			->thisMonth()
			->findAll();

		/** @var SpecOrder[] $specOrders */
		$specOrders = SpecOrder::model()
			->active()
			->with('customer.addresses')
			->thisMonth()
			->findAll();

		// -- суммируем бетон за каждый день
		$ordersEachDay = [];
		foreach ($concreteOrders as $concreteOrder) {
			$ordersEachDay[$concreteOrder->getDateWoTime()][] = $concreteOrder->volume;
		}

		$orders = [];
		foreach ($ordersEachDay as $day => $orderEachDay) {
			$orders[] = [
				'title' => 'Бетон: ' . array_sum($orderEachDay) . ' м3',
				'start' => $day,
				'end'   => $day,
			];
		}
		// -- -- -- --

		// -- суммируем бетон за каждый день
		$ordersEachDay = [];
		foreach ($specOrders as $specOrder) {
			$ordersEachDay[$specOrder->getDateWoTime()][] = 1;
		}

		foreach ($ordersEachDay as $day => $orderEachDay) {
			$orders[] = [
				'title' => 'Спецтехника: ' . array_sum($orderEachDay),
				'start' => $day,
				'end'   => $day,
			];
		}
		// -- -- -- --

		$this->renderJson($orders);
	}

	public function actionFillCementNextDay() {
		$prevDay = date('Y-m-d', time() - 24 * 3600);
		$today = date('Y-m-d');
		$nextDay = date('Y-m-d', time() + 24 * 3600);

		$citySilages = [
			Cement::CITY_VLADIVOSTOK => [
				1,
				2,
				3,
			],
		];
		foreach ($citySilages as $city => $silages) {
			/** @var bool $allSilagesDown Означает, что все силосы "заброшены" менеджером и скрипт нужно остановить. Ежели хоть один силос работает (проверка ниже), ставим false */
			$allSilagesDown = true;
			// Сперва запускаем проверку на предмет "застоенности" всего завода по всем его силосам
			foreach ($silages as $silage) {
				$cementPrevDay = Cement::model()->findByAttributes([
					'city'   => $city,
					'date'   => $prevDay,
					'silage' => $silage,
				]);
				$cement = Cement::model()->findByAttributes([
					'city'   => $city,
					'date'   => $today,
					'silage' => $silage,
				]);

				// если нет вчерашней записи ИЛИ сегодня и вчера одинаковые остатки, а также сегодня нулевые сдвиги в силосах,
				// значит сегодня была добавлена "нулевая" запись и на завтра такую делать не надо
				if ($cement && (!$cementPrevDay || $cementPrevDay->remains == $cement->remains)
					&& $cement->outgo == 0 && $cement->overhead == 0 && $cement->difference == 0) {
					echo "Запись цемента #{$cement->id} 'заброшена' \n";
				} else {
					// Ежели хоть одна запись цемента "активна", запускаем скрипт для всего завода
					$allSilagesDown = false;
					echo "Запись цемента #{$cement->id} 'активна' \n";
					break;
				}
			}

			if ($allSilagesDown) {
				echo "Пропускаем завод в городе {$city} \n";
				continue;
			}

			foreach ($silages as $silage) {
				$cement = Cement::model()->findByAttributes([
					'city'   => $city,
					'date'   => $today,
					'silage' => $silage,
				]);

				$cementNextDay = new Cement();
				$cementNextDay->city = $city;
				$cementNextDay->silage = $silage;
				$cementNextDay->remains = $cement? $cement->remains: 0;
				$cementNextDay->date = $nextDay;
				$cementNextDay->outgo = 0;
				$cementNextDay->overhead = 0;
				$cementNextDay->difference = 0;
				$cementNextDay->driverId = 4;
				if ($cementNextDay->save(false)) {
					echo "Запись цемента #{$cementNextDay->id} сохранена \n";
				}
			}
		}
	}
}
