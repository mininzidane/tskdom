/**
 * Created by PhpStorm.
 * User: Lukin Anton
 * Date: 12.12.2014
 * Time: 2:48
 */

$(function () {

	/**
	 * Массив цен, rent - цены аренды, sale - цены продажи
	 *
	 * @type {{frameScaffold: {rent: Array, sale: Array}, formWork: {rent: Array, sale: Array}, smallPanelFormWork: {rent: Array, sale: Array}, clampScaffold: {rent: Array, sale: Array}}}
	 */
	var prices = {
		// Рамные леса
		frameScaffold: {
			rent: [
				1, 2, 3, 4
			],
			rentPrice: 60,
			sale: [
				800, 750, 530, 280
			]
		},

		// Рамные леса - доп. опции
		frameScaffoldAdditional: {
			rent: [
				1, 2, 3, 4, 4, 4
			],
			sale: [
				100, 150, 2400, 510, 530, 280
			]
		},

		// Опалубка перекрытий
		formWork: {
			rent: [
				20, 20, 40, 10, 20
			],
			sale: [
				980, 20 , 450, 180, 1900
			],
			rackSale: {
				'0.7-1.2': 620,
				'0.9-1.6': 650,
				'1.7-3.1': 900,
				'1.8-3.2': 1100,
				'2-3.7'  : 1000,
				'2.4-3.8': 1200,
				'2.5-4.2': 1200,
				'2.9-4.5': 1300,
				'3.2-5.8': 1800
			}
		},

		// Мелкощитовая опалубка
		smallPanelFormWork: {
			rent: [
				0.06, // Клин
				0.27, // Крюк большой
				0.21, // Крюк малый
				10 // Труба 3м
			],
			rentPrice: 500,
			sale: [
				0.06, // Клин
				0.27, // Крюк большой
				0.21, // Крюк малый
				1 // Труба 3м
			],
			// Щиты аренда
			shieldRent: {
				shield600x1200: 10,
				shield600x1500: 10,
				shield600x1800: 10,
				shield200x1800: 10,
				shield300x1800: 10,
				shield50x1800: 10,
				shield100x1800: 10,
				shield150x1800: 10, // 200x1800 пропускаем, т.к. он выше есть
				shield250x1800: 10, // 300x1800 пропускаем, т.к. он выше есть
				shield350x1800: 10,
				shield400x1800: 10,
				shield450x1800: 10,
				shield500x1800: 10,
				shield550x1800: 10
			},
			// Щиты продажа
			shieldSale: {
				shield600x1200: 42,
				shield600x1500: 51,
				shield600x1800: 70,
				shield200x1800: 45,
				shield300x1800: 49,

				shield50x1800: 0,
				shield100x1800: 45,
				shield150x1800: 45, // 200x1800 пропускаем, т.к. он выше есть
				shield250x1800: 47, // 300x1800 пропускаем, т.к. он выше есть
				shield350x1800: 52,
				shield400x1800: 54,
				shield450x1800: 58,
				shield500x1800: 61,
				shield550x1800: 70
			},
			// Тяга аренда
			rodRent: {
				150: 10,
				200: 10,
				250: 10,
				300: 10,
				350: 10,
				400: 10,
				450: 10,
				500: 10,
				550: 10,
				600: 10
			},
			// Тяга продажа
			rodSale: {
				150: 0.25,
				200: 0.25,
				250: 0.28,
				300: 0.32,
				350: 0.38,
				400: 0.41,
				450: 0.41,
				500: 0.49,
				550: 0.49,
				600: 0.57
			}
		},

		// Леса хомутовые
		clampScaffold: {
			rent: [
				20, 20, 40, 10, 20
			],
			rentPrice: 200,
			sale: [
				420, 800, 70, 80, 80
			]
		}
	};

	var calc = {

		/**
		 * Меняем запятую на точку в числах
		 *
		 * @param {string} value
		 * @returns {number}
		 */
		replaceComma: function(value) {
			return parseFloat(value.toString().replace(',', '.'));
		},

		/**
		 * Рамные леса
		 *
		 * @param {int} height Высота
		 * @param {int} length Длина
		 * @returns {{result: {stairs: *, communicating: number, diagonal: number, horizontal: number}, additionalResult: {horizontal: number, diagonal: number, heelSupport: number, screwSupport: number, bracket: number, flooring: number}}}
		 */
		frameScaffold: function (height, length) {
			height = this.replaceComma(height);
			length = this.replaceComma(length);

			height = parseFloat(height);
			length = parseFloat(length);

			// first table
			var nStairs = Math.ceil(height),
				nCommunicating = Math.ceil((length / 3 + 1) * height / 2 - height),
				nDiagonal = Math.ceil(height + (nCommunicating - height / 2) / 2),
				nHorizontal = Math.ceil(nCommunicating + height / 2),
				result = {
					stairs: nStairs,
					communicating: nCommunicating,
					diagonal: nDiagonal,
					horizontal: nHorizontal,
					square: (height * length).toFixed(2)
				};

			// second table
			nDiagonal = Math.ceil(nDiagonal - nStairs);
			var heelSupport = Math.ceil(length / 3 * 2 + 2),
				screwSupport = Math.ceil(length / 3 * 2 + 2),
				bracket = Math.ceil(height * length / 25),
				flooring = Math.ceil(length * (height / 2 - 1)),
				additionalResult = {
					horizontal: nHorizontal,
					diagonal: nDiagonal,
					heelSupport: heelSupport,
					screwSupport: screwSupport,
					bracket: bracket,
					flooring: flooring
				};

			return {
				result: result,
				additionalResult: additionalResult
			};
		},

		/**
		 * Опалубка перекрытий
		 *
		 * @param {int} square Площадь
		 */
		formWork: function(square) {
			square = this.replaceComma(square);

			var rack = Math.round(square / 1.5),
				uniSpoon = Math.round(rack/ 2),
				tripod = Math.round(rack/ 2),
				beam = Math.floor(square * 3.5),
				plywood = Math.round(square / 2.97);

			return {
				rack: rack,
				uniSpoon: uniSpoon,
				tripod: tripod,
				beam: beam,
				plywood: plywood
			}
		},

		/**
		 * Получает варианты для размеров стойки по высоте
		 *
		 * @param {float} height Высота стойки
		 * @returns {Array}
		 */
		getRackSize: function(height) {
			height = this.replaceComma(height);
			height = parseFloat((height - 0.4).toFixed(1));
			var result = [],
				intervals = [
					[0.7, 1.2],
					[0.9, 1.6],
					[1.7, 3.1],
					[1.8, 3.2],
					[2.0, 3.7],
					[2.4, 3.8],
					[2.5, 4.2],
					[2.9, 4.5],
					[3.2, 5.8]
				];

			$.each(intervals, function(i, interval) {
				if (height >= interval[0] && height <= interval[1]) {
					result.push(interval);
				}
			});

			return result;
		},

		/**
		 * Возвращает Сумму продажи, сумму аренды и HTML таблицы результатов расчета по переданным данным
		 *
		 * @param {Array} names
		 * @param {Array} counts
		 * @param {Array} prices
		 * @param {boolean} [useCheckboxes]
		 * @param {Number|String} rentPrice
		 */
		getResults: function(names, counts, prices, rentPrice, useCheckboxes) {
			if (typeof useCheckboxes == 'undefined') {
				useCheckboxes = false;
			}

			var html = '<table class="table">',
				sum = 0,
				rentSum = 0;

			html += '<tr>' +
						'<th>Наименование</th>' +
						'<th>Кол-во, шт</th>' +
						'<th>Цена за ед., руб (мелкощитовая в $)</th>' +
						'<th>Сумма, руб (мелкощитовая в $)</th>' +
						//'<th>Цена аренды, руб</th>' +
						(useCheckboxes? '<th></th>': '') +
					'</tr>';

			$.each(names, function(i, name) {
				sum += counts[i] * prices[i];
				//rentSum += counts[i] * rentPrices[i];
				html += '<tr>' +
							'<td>' + name + '</td>' +
							'<td>' + counts[i] + '</td>' +
							'<td>' + prices[i] + '</td>' +
							'<td class="js-row-sum">' + (counts[i] * prices[i]).toFixed(2) + '</td>' +
							//'<td class="js-row-rent-sum">' + (counts[i] * rentPrice[i]) + '</td>' +
							(useCheckboxes? '<td><input type="checkbox" class="js-use-additional-option" checked></td>': '') +
						'</tr>';
			});

			html += '</table>';

			return {
				sum: sum,
				rentSum: rentPrice,
				table: html
			};
		},

		/**
		 * Проверяет заполненность переданных значений
		 *
		 * @returns {boolean}
		 */
		checkFields: function() {
			var success = true;

			$.each(arguments, function(i, item) {
				if (item === '') {
					success = false;
				}
			});

			if (success === false) {
				alert('Заполните все поля');
			}
			return success;
		},

		/**
		 * Определяем, целое ли число
		 *
		 * @param {number|string} number
		 * @returns {boolean}
		 */
		isInteger: function(number) {
			return (number ^ 0) === number;
		},

		/**
		 * Мелкощитовая опалубка
		 *
		 * @param height Высота
		 * @param length Длина
		 */
		smallPanelFormWork: function(height, length) {
			height = this.replaceComma(height);
			length = this.replaceComma(length);

			height = parseFloat(height);
			length = parseFloat(length);

			var shieldCount = 0,
				shield600x1200 = 0,
				shield600x1500 = 0,
				shield600x1800 = 0,
				shield200x1800 = 0,
				shield300x1800 = 0,
				shieldHx1800 = 0,
				nShieldM;

			// -- Высота >= 1.8
			if (height >= 1.8) {
				shieldCount = length / 0.6 * height / 1.8 * 2;

				if (calc.isInteger(height / 1.8)) {
					shield600x1800 = shieldCount;
				} else {
					var shiftedHeight = height - 1.8 * Math.floor(height / 1.8);// для определения, какие щиты юзаем
					nShieldM = Math.ceil(length / 0.6 * 2); // число щитов нужного размера

					if (shiftedHeight <= 1.2) {
						shield600x1200 = nShieldM;
					} else if ((1.2 < shiftedHeight) && (shiftedHeight <= 1.5)) {
						shield600x1500 = nShieldM;
					} else if (1.5 < shiftedHeight && shiftedHeight <= 1.8) {
						shield600x1800 = nShieldM;
					}

					shield600x1800 += nShieldM * Math.floor(height / 1.8);
				}
			}
			// -- -- -- --

			// -- 1.5 <= Высота < 1.8
			else if ((1.5 <= height) && (height < 1.8)) {
				shieldCount = length / 0.6 * height / 1.5 * 2;

				if (calc.isInteger(height / 1.5)) {
					shield600x1500 = shieldCount;
				} else {
					nShieldM = Math.ceil(length / 1.8 * 2);

					if ((1.7 <= height) && (height < 1.8)) {
						shield300x1800 = nShieldM;
					} else if ((1.5 < height) && (height < 1.7)) {
						shield200x1800 = nShieldM;
					}

					shield600x1500 += Math.ceil(2 * length / 0.6 * Math.floor(height / 1.5));
				}
			}
			// -- -- -- --

			// -- 1.2 <= Высота < 1.5
			else if ((1.2 <= height) && (height < 1.5)) {
				shieldCount = length / 0.6 * height / 1.2 * 2;

				if (calc.isInteger(height / 1.2)) {
					shield600x1200 = shieldCount;
				} else {
					nShieldM = Math.ceil(length / 1.8 * 2); // вот здесь не факт, из предыдущей формулы

					if ((1.4 <= height) && (height <= 1.5)) {
						shield300x1800 = nShieldM;
					} else if ((1.2 < height) && (height < 1.4)) {
						shield200x1800 = nShieldM;
					}

					shield600x1200 += Math.ceil(length / 0.6 * 2) * Math.floor(height / 1.2);
				}
			}
			// -- -- -- --

			// -- 0.6 < Высота < 1.2
			else if ((0.6 < height) && (height < 1.2)) {
				shield600x1200 = Math.ceil(2 * length / 0.6);
			}
			// -- -- -- --

			// -- Высота <= 0.6
			else if (height <= 0.6) {
				shieldHx1800 = Math.ceil(2 * length / 1.8);
			}
			// -- -- -- --

			var shieldTotalCount = shield600x1200 + shield600x1500 + shield600x1800 + shield200x1800 + shield300x1800 + shieldHx1800,
				wedge = shieldTotalCount * 12, // клин
				rod = shieldTotalCount * 7, // тяга + толщина
				pipeHorizontal = Math.floor(length / 2.5 * 4 * height / 1.5),// с трубой хз, суммируем
				pipeVertical = Math.floor(length / 1.5 * height / 2.5),
				hookSmall = shieldTotalCount * 2, // крюк малый
				hookBig = pipeVertical * 3; // крюк гигантский

			return {
				shield600x1200: shield600x1200,
				shield600x1500: shield600x1500,
				shield600x1800: shield600x1800,
				shield200x1800: shield200x1800,
				shield300x1800: shield300x1800,
				shieldHx1800: shieldHx1800,
				wedge: wedge,
				rod: rod,
				pipe: pipeHorizontal + pipeVertical,
				hookSmall: hookSmall,
				hookBig: hookBig
			};
		},

		/**
		 * Леса хомутовые
		 *
		 * @param {int} height
		 * @param {int} length
		 * @returns {{pipe2: number, pipe4: number, finger: number, lockFixed: number, lockTurned: number}}
		 */
		clampScaffold: function(height, length) {
			height = this.replaceComma(height);
			length = this.replaceComma(length);

			height = parseFloat(height);
			length = parseFloat(length);

			var pipes4Whole = Math.floor(height / 4),
				pipes4Rest = height / 4 - pipes4Whole,
				pipes4_1 = Math.ceil(pipes4Whole * 2 * length / 3),
				pipes2_1 = Math.ceil(pipes4Rest / 2 * 2 * length / 3),
				pipes4_2 = Math.ceil(2 * (length / 3 * height / 2 - length / 3 * height / 2 / 4)),
				pipes2_2 = Math.ceil(height / 2 * length / 3),

				pipe2 = pipes2_1 + pipes2_2,
				pipe4 = pipes4_1 + pipes4_2,
				finger = Math.ceil((pipes4Whole - 1) * 2 * length / 3 + (pipes4Rest - 1) * 2 * length / 3),
				lockFixed = pipes2_2 * 2,
				lockTurned = pipes4_2 * 2;

			// если высота < 4
			if (height < 4) {
				var pipes2Whole = Math.floor(height / 2);
				var pipes2Rest = height / 2 - pipes2Whole;
				var pipes22_1 = Math.ceil(pipes2Whole * 2 * length / 3);

				pipes2_1 = Math.ceil(pipes2Rest / 2 * 2 * length / 3);

				pipe2 = pipes2_1 + pipes22_1;
				pipe4 = 0;
				finger = Math.ceil((pipes2Whole - 1) * 2 * length / 3 + (pipes2Rest - 1) * 2 * length / 3);
				lockFixed = 0;
				lockTurned = 0;
			}

			return {
				pipe2: pipe2,
				pipe4: pipe4,
				finger: finger,
				lockFixed: lockFixed,
				lockTurned: lockTurned
			}
		}
	};

	var $body = $('body');

	// Считаем леса рамные
	$body.on('click', '.js-frame-scaffold-calculate', function() {
		var $this = $(this),
			$container = $this.parent().find('.js-calc-container').removeClass('hide'),
			$resultTable = $container.find('.js-calc-table-result'),
			$additionalTable = $container.find('.js-calc-table-additional'),
			$sum = $container.find('.js-calc-sum-result'),
			$rentSum = $container.find('.js-calc-rentSum-result'),
			$square = $container.find('.js-calc-square-result'),

			height = $('#frame-scaffold-calculate-height').val(),
			length = $('#frame-scaffold-calculate-length').val();

		if (!calc.checkFields(height, length)) {
			return false;
		}

		height = calc.replaceComma(height);
		length = calc.replaceComma(length);
		var square = height * length;

		var frameScaffoldCounts = calc.frameScaffold(height, length),
			result = calc.getResults(
				[
					'Рама с лестницей',
					'Рама проходная',
					'Диагональ',
					'Горизонталь'
				],
				[
					frameScaffoldCounts.result.stairs,
					frameScaffoldCounts.result.communicating,
					frameScaffoldCounts.result.diagonal,
					frameScaffoldCounts.result.horizontal
				],
				prices.frameScaffold.sale,
				(prices.frameScaffold.rentPrice * square).toFixed(2)
			),
			additionalTable = calc.getResults(
				[
					'Опорная пята',
					'Кронштейн',
					'Метал. настил',
					'Винтовая опора',
					'Диагональ',
					'Горизонталь'
				],
				[
					frameScaffoldCounts.additionalResult.heelSupport,
					frameScaffoldCounts.additionalResult.bracket,
					frameScaffoldCounts.additionalResult.flooring,
					frameScaffoldCounts.additionalResult.screwSupport,
					frameScaffoldCounts.additionalResult.diagonal,
					frameScaffoldCounts.additionalResult.horizontal
				],
				prices.frameScaffoldAdditional.sale,
				0,
				true
			);

		$resultTable.html(result.table);
		$additionalTable.html(additionalTable.table);

		$square.html(square.toFixed(2));
		$sum.html(result.sum);
		$rentSum.html(result.rentSum);
		$container.find('.js-use-additional-option:first').trigger('change');
		return false;
	});

	// чекбокс для включения доп элементов в результат
	$body.on('change', '.js-use-additional-option', function() {
		var $this = $(this),
			$container = $this.closest('.js-calc-container'),
			rentSum = parseFloat($container.find('.js-calc-rentSum-result').text()),
			sum = parseFloat($container.find('.js-calc-sum-result').text()),
			$sumCells = $this.closest('table').find('tr:has(:checked)').find('.js-row-sum'),
			$rentSumCells = $this.closest('table').find('tr:has(:checked)').find('.js-row-rent-sum'),

			additionalRentSum = 0,
			additionalSum = 0;

		$sumCells.each(function() {
			additionalSum += parseFloat($(this).text());
		});
		$rentSumCells.each(function() {
			additionalRentSum += parseFloat($(this).text());
		});
		$container.find('.js-calc-additionalRentSum-result').html(rentSum + additionalRentSum);
		$container.find('.js-calc-additionalSum-result').html(sum + additionalSum);
		return false;
	});

	// Расчет размеров стойки
	$body.on('change', '#formWork-calculate-height', function() {
		var height = $(this).val().replace(',', '.'),
			$size = $('#formWork-calculate-size').empty();

		$.each(calc.getRackSize(height), function(i, size) {
			$size.append('<option>' + size.join('-') + '</option>');
		});
	});

	// Расчет опалубки перекрытий
	$body.on('click', '.js-formWork-calculate', function() {
		var $this = $(this),
			$container = $this.parent().find('.js-calc-container').removeClass('hide'),
			$resultTable = $container.find('.js-calc-table-result'),
			$sum = $container.find('.js-calc-sum-result'),
			$rentSum = $container.find('.js-calc-rentSum-result'),
			square = $('#formWork-calculate-square').val(),
			height = $('#formWork-calculate-height').val(),
			rackSize = $('#formWork-calculate-size').val();

		if (!calc.checkFields(square, height, rackSize)) {
			return false;
		}

		square = calc.replaceComma(square);
		height = calc.replaceComma(height);

		var pricesFormWorkSale = prices.formWork.sale.slice(0);
		pricesFormWorkSale[1] = prices.formWork.rackSale[rackSize];
		var formWorkCounts = calc.formWork(square),
			result = calc.getResults([
				'Балка 3,0 м',
				'Стойка ' + rackSize,
				'Тренога',
				'Унивилка',
				'Фанера'
			], [
				formWorkCounts.beam,
				formWorkCounts.rack,
				formWorkCounts.tripod,
				formWorkCounts.uniSpoon,
				formWorkCounts.plywood
			],
				pricesFormWorkSale,
				prices.formWork.rent);

		$resultTable.html(result.table);
		$sum.html(result.sum);
		$rentSum.html(result.rentSum);
		return false;
	});

	// Расчет мелкощитовой опалубки
	$body.on('click', '.js-smallPanelFormWork-calculate', function() {
		var $this = $(this),
			$container = $this.parent().find('.js-calc-container').removeClass('hide'),
			$resultTable = $container.find('.js-calc-table-result'),
			$sum = $container.find('.js-calc-sum-result'),
			$rentSum = $container.find('.js-calc-rentSum-result'),
			$square = $container.find('.js-calc-square-result'),

			height = $('#smallPanelFormWork-calculate-height').val(),
			length = $('#smallPanelFormWork-calculate-length').val(),
			thickness = $('#smallPanelFormWork-calculate-thickness').val();

		if (!calc.checkFields(height, length)) {
			return false;
		}

		height = calc.replaceComma(height);
		length = calc.replaceComma(length);
		var square = height * length;

		// -- Округляем до шага в 50 мм
		var heightLabel = 50; // если больше значения в 600мм, то ставим минималку, просто так
		if (height <= 0.6) {
			var heightMM = height * 1000; // высота в ММ
			heightLabel = (Math.floor(calc.isInteger(heightMM / 50)? (heightMM / 50 - 1): (heightMM / 50)) + 1) * 50; // округляем дробную часть до шага 50мм
		}
		if (height < 0.2) {
			heightLabel = 200;
		}
		if (height > 0.6) {
			heightLabel = 450;
		}
		
		// -- -- -- --

		var formWorkCounts = calc.smallPanelFormWork(height, length),
			result = calc.getResults([
					'Щит 600х1200',
					'Щит 600х1500',
					'Щит 600х1800',
					'Щит 200х1800',
					'Щит 300х1800',
					'Щит ' + heightLabel + 'х1800',
					'Клин',
					'Крюк большой',
					'Крюк малый',
					'Труба 3м',
					'Тяга ' + thickness
				], [
					formWorkCounts.shield600x1200,
					formWorkCounts.shield600x1500,
					formWorkCounts.shield600x1800,
					formWorkCounts.shield200x1800,
					formWorkCounts.shield300x1800,
					formWorkCounts.shieldHx1800,
					formWorkCounts.wedge,
					formWorkCounts.hookBig,
					formWorkCounts.hookSmall,
					formWorkCounts.pipe,
					formWorkCounts.rod
				], [
						prices.smallPanelFormWork.shieldSale.shield600x1200,
						prices.smallPanelFormWork.shieldSale.shield600x1500,
						prices.smallPanelFormWork.shieldSale.shield600x1800,
						prices.smallPanelFormWork.shieldSale.shield200x1800,
						prices.smallPanelFormWork.shieldSale.shield300x1800,
						prices.smallPanelFormWork.shieldSale['shield' + heightLabel + 'x1800']
				].concat(prices.smallPanelFormWork.sale.concat(prices.smallPanelFormWork.rodSale[thickness])),
				(prices.smallPanelFormWork.rentPrice * square).toFixed(2)
			);

		$resultTable.html(result.table);
		$sum.html(result.sum.toFixed(2));
		$rentSum.html(result.rentSum);
		$square.html(square.toFixed(2));
		return false;
	});

	// расчет хомутовых лесов
	$body.on('click', '.js-clampScaffold-calculate', function() {
		var $this = $(this),
			$container = $this.parent().find('.js-calc-container').removeClass('hide'),
			$resultTable = $container.find('.js-calc-table-result'),
			$sum = $container.find('.js-calc-sum-result'),
			$rentSum = $container.find('.js-calc-rentSum-result'),
			$square = $container.find('.js-calc-square-result'),

			height = $('#clampScaffold-calculate-height').val(),
			length = $('#clampScaffold-calculate-length').val();

		if (!calc.checkFields(height, length)) {
			return false;
		}

		height = calc.replaceComma(height);
		length = calc.replaceComma(length);
		var square = Math.round(height * length);

		var clampScaffoldResult = calc.clampScaffold(height, length),
			result = calc.getResults([
				'Труба 2м',
				'Труба 4м',
				'Палец',
				'Замок фиксированный',
				'Замок поворотный'
			], [
				clampScaffoldResult.pipe2,
				clampScaffoldResult.pipe4,
				clampScaffoldResult.finger,
				clampScaffoldResult.lockFixed,
				clampScaffoldResult.lockTurned
			],
				prices.clampScaffold.sale,
				(prices.clampScaffold.rentPrice * square).toFixed(2)
			);

		$resultTable.html(result.table);
		$sum.html(result.sum);
		$rentSum.html(result.rentSum);
		$square.html(square);
		return false;
	})
});
