<?php

class m150118_094450_change_concrete_order_volume_from_int_to_float extends DbMigration {

	public function safeUp() {
		$this->alterColumn('concrete_order', 'volume', 'FLOAT NULL DEFAULT NULL');
	}

	public function safeDown() {
		$this->alterColumn('concrete_order', 'volume', 'INT(11) UNSIGNED NULL DEFAULT NULL');
	}
}
